#include <thread>

#include <TH1I.h>
#include <TH1F.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TAxis.h>
#include <TGaxis.h>

#include "monitor.h"

Monitor::Monitor(const char *fileName) :
    n_events(0),
    busy(false),
    last_updated(time(NULL)),
    last_saved(time(NULL)) 
{
    // Clearing the event data structure
    bzero(&this->event, sizeof(TD_monitor_event));
    // Constructing the output file name
    this->fileNameBase = std::string(fileName);
    // Changing format of the output file name
    int dot_position = this->fileNameBase.rfind('.');
    this->fileNameBase.erase(dot_position);
    printf("Starting Live Monitor with output filename: %s\n", this->fileNameBase.c_str());
    this->setStyle();
    this->bookHistos();
    this->initCanvas();
    this->updateCanvas();
}

void Monitor::initCanvas() {
    this->app = new TApplication("monitor", 0, 0);
    this->sys = gSystem;
    int canvas_size[2] = {1920, 1000};   // [width, height]
    this->canvas = new TCanvas("canvas", "Event display", canvas_size[0], canvas_size[1]);
    this->canvas->Divide(3, 2);
    this->canvas->GetPad(1)->Divide(2, 1);
    std::vector<int> channels(0);
    
    // Plot the Q status histogram
    this->canvas->GetPad(1)->cd(1);
    this->histos["h_status_q"]->Draw();
    this->histos["h_status_q"]->SetStats(0);

    // Plot the Pattern Unit output
    channels.clear();
    channels.push_back(0);
    this->canvas->GetPad(1)->cd(2);
    this->drawHistos(0, channels, "h_pattern_unit_ch%d", 0);
    
    // Plot the TDC output
    channels.clear();
    channels.push_back(7);
    this->drawHistos(2, channels, "h_tdc_ch%d");
    gPad->SetLogy();
    gPad->SetGridx();

    // Plot the Scaler c257 output
    channels.clear();
    channels.push_back(15);
    this->drawHistos(3, channels, "h_scaler_c257_ch%d");
    gPad->SetLogy();

    // Plot the ADC LC2249W output
    channels.clear();
    channels.push_back(11);
    this->drawHistos(4, channels, "h_adc_lc2249w_ch%d");

    // Plot the Scaler v560n output
    channels.clear();
    channels.push_back(0);
    this->drawHistos(5, channels, "h_scaler_v560n_ch%d", 0);

    // Plot the Dead time
    this->canvas->cd(6);
    TH1* histo = this->histos["h_dead_time"];
    histo->Draw();
}

void Monitor::drawHistos(int pad, const std::vector<int> &channels, const char *nameTemplate, int stats) {
    if (pad > 0) {
        this->canvas->cd(pad);
    }
    char buffer[50];
    for (size_t ch=0; ch < channels.size(); ++ch) {
        int channel = channels.at(ch);
        sprintf(buffer, nameTemplate, channel);
        std::string name(buffer);
        TH1* histo = this->histos[name];
        histo->SetLineColor(COLORS[ch]);
        histo->SetFillColor(FILL_COLORS[ch]);
        histo->Draw(ch == 0 ? "" : "same");
        histo->SetStats(stats);
    }
}

void Monitor::bookHistos() {
    printf("Booking histograms\n");
    char buffer[50];

    // Histogram with status Q of each module
    std::string name = "h_status_q";
    this->histos[name] = new TH1I(name.c_str(), "Q status;Module index [0-15];# events", 16, 0, 16);

    // Histogram for TDC output from each channel
    for (size_t channel=0; channel<8; ++channel) {
        sprintf(buffer, "h_tdc_ch%d", int(channel));
        name = std::string(buffer);
        this->histos[name] = new TH1I(name.c_str(), "TDC [2228A];# channels;# events", 400, 0, 4000);
    }
    // Histogram for ADC LC2249A output from each channel
    for (size_t channel=0; channel<12; ++channel) {
        sprintf(buffer, "h_adc_lc2249a_ch%d", int(channel));
        name = std::string(buffer);
        this->histos[name] = new TH1I(name.c_str(), "ADC [2249A];# channels;# events", 205, 0, 2050);
    }
    // Histogram for ADC LC2249W output from each channel
    for (size_t channel=0; channel<12; ++channel) {
        sprintf(buffer, "h_adc_lc2249w_ch%d", int(channel));
        name = std::string(buffer);
        this->histos[name] = new TH1I(name.c_str(), "ADC [2249W];# channels;# events", 205, 0, 2050);
    }
    // Histogram for Pattern Unit output from each channel
    for (size_t channel=0; channel<2; ++channel) {
        sprintf(buffer, "h_pattern_unit_ch%d", int(channel));
        name = std::string(buffer);
        this->histos[name] = new TH1I(name.c_str(), "Pattern Unit [V259N];word bits;# events", 16, 1, 17);
    }
    // Histogram for Scaler output from each channel
    for (size_t channel=0; channel<16; ++channel) {
        sprintf(buffer, "h_scaler_c257_ch%d", int(channel));
        name = std::string(buffer);
        this->histos[name] = new TH1I(name.c_str(), "Scaler (TDC) [C257];# channels;# events", 110, 0, 110);
    }
    // Histogram for Dead time
    sprintf(buffer, "h_dead_time");
    name = std::string(buffer);
    this->histos[name] = new TH1I(name.c_str(), "Dead time;1 - recorded / triggered;# events", 101, 0.0, 1.01);

    // Histogram for Scaler output from each channel
    for (size_t channel=0; channel<16; ++channel) {
        sprintf(buffer, "h_scaler_v560n_ch%d", int(channel));
        name = std::string(buffer);
        this->histos[name] = new TH1I(name.c_str(), "Scalers [v560n];type of signal;# events", 2, 0, 2);
        TAxis* axis = this->histos[name]->GetXaxis();
        axis->SetBinLabel(1, "non-inhibited");
        axis->SetBinLabel(2, "inhibited");
    }

    // Histogram for Scaler output from each channel
    for (size_t channel=0; channel<16; ++channel) {
        sprintf(buffer, "h_scaler_v560n2_ch%d", int(channel));
        name = std::string(buffer);
        this->histos[name] = new TH1I(name.c_str(), "Scaler [v560n];# channels;# events", 1000, 0, 2000);
    }
}

void Monitor::updateCanvas() {
    // Doing nothing if still busy with an update from a past event
    if (this->busy) return;
    this->busy = true;
    // Doing nothing if no events were taken
    // if (this->n_events > 0) {
    // // Redrawing axes over histograms
    //     for (size_t i=1; i<=3*2; ++i) {
    //         this->canvas->GetPad(i)->RedrawAxis();
    //     }
    // }
    // Updating the canvas
    this->canvas->Update();
    this->canvas->Draw();
    this->sys->ProcessEvents();
    this->last_updated = time(NULL);
    this->busy = false;
}

void Monitor::fillEvent() {
    char buffer[50];
    std::string name;
    const TD_monitor_event& event = this->event;
    // Filling Q Status histogram
    name = "h_status_q";
    for (size_t bit = 0; bit < 16; ++bit) {
        if (event.dataErrMask >> (16 - 1 - bit) & 0b1) {
            this->histos[name]->Fill(bit);
        }
    }
    // Filling Pattern Unit histogram
    for (size_t ch = 0; ch < 2; ++ch) {
        sprintf(buffer, "h_pattern_unit_ch%d", int(ch));
        name = std::string(buffer);
        for (size_t bit = 1; bit <= 16; ++bit) {
            if (event.data_pattern_unit[ch] >> (bit - 1) & 0b1) {
                this->histos[name]->Fill(bit);
            }
        }
    }
    // Filling histograms for 8 TDC channels
    // for (size_t ch = 0; ch < 8; ++ch) {
    for (size_t ch = 7; ch < 8; ++ch) {
        sprintf(buffer, "h_tdc_ch%d", int(ch));
        name = std::string(buffer);
        this->histos[name]->Fill(event.data_tdc[ch]);
    }
    // // Filling histograms for 12 ADC LC2249A channels
    // for (size_t ch = 0; ch < 12; ++ch) {
    //     sprintf(buffer, "h_adc_lc2249a_ch%d", int(ch));
    //     name = std::string(buffer);
    //     this->histos[name]->Fill(event.data_adc_lc2249a[ch]);
    // }
    // Filling histograms for 12 ADC LC2249W channels
    // for (size_t ch = 0; ch < 12; ++ch) {
    for (size_t ch = 10; ch < 12; ++ch) {
        sprintf(buffer, "h_adc_lc2249w_ch%d", int(ch));
        name = std::string(buffer);
        this->histos[name]->Fill(event.data_adc_lc2249w[ch]);
    }
    // Filling histograms for 8 channels of Scaler c257
    // for (size_t ch = 0; ch < 16; ++ch) {
    for (size_t ch = 15; ch < 16; ++ch) {
        sprintf(buffer, "h_scaler_c257_ch%d", int(ch));
        name = std::string(buffer);
        this->histos[name]->Fill(event.data_scaler_c257[ch]);
    }
    // Filling histograms for 16 channels of Scaler v560n Inhibited
    // for (size_t ch = 0; ch < 16; ++ch) {
    for (size_t ch = 0; ch < 1; ++ch) {
        sprintf(buffer, "h_scaler_v560n_ch%d", int(ch));
        name = std::string(buffer);
        this->histos[name]->Reset();
        this->histos[name]->Fill("non-inhibited", Double_t(event.data_scaler_v560n[ch]));
        this->histos[name]->Fill("inhibited", Double_t(event.data_scaler_v560n2[ch]));
    }
    // // Filling histograms for 16 channels of Scaler v560n
    // for (size_t ch = 0; ch < 16; ++ch) {
    //     sprintf(buffer, "h_scaler_v560n2_ch%d", int(ch));
    //     name = std::string(buffer);
    //     this->histos[name]->Fill(event.data_scaler_v560n2[ch]);
    // }
    // Filling histogram for Dead time
    sprintf(buffer, "h_dead_time");
    name = std::string(buffer);
    this->histos[name]->Fill(1.0 - float(event.data_scaler_v560n2[0]) / float(event.data_scaler_v560n[0]));


    this->n_events++;
    // Updating the canvas at given rate
    time_t now = time(NULL);

    if (MONITOR_LIVE_UPDATES && difftime(now, this->last_updated) >= MONITOR_LIVE_UPDATE_PERIOD) {
        // Executing canvas update in a separate thread to avoid blocking of data taking
        std::thread t(&Monitor::updateCanvas, this);
        t.detach();
    }
    else if (difftime(now, this->last_saved) >= MONITOR_SAVE_PERIOD) {
        this->updateCanvas();
        // Saving monitor files
        bzero(&buffer, sizeof(buffer));
        struct tm* timestamp = localtime(&now);
        strftime(&buffer[0], sizeof(buffer), "_at_%Y%m%d_%H%M%S", timestamp);
        this->saveFile(buffer);   
    }
}

void Monitor::saveFile(const char *append) {
    // Update the canvas to be safe
    while (true) {
        if (this->busy) continue;
        this->updateCanvas();
        break;
    }
    // Writing histograms to a ROOT file
    std::string fileName = this->fileNameBase + append + ".root";
    printf("Writing %d histograms to file: %s\n", (int)this->histos.size(), fileName.c_str());
    TFile *fileOut = new TFile(fileName.c_str(), "RECREATE");
    for (std::map<std::string, TH1*>::const_iterator it = this->histos.begin(); it != this->histos.end(); ++it) {   
        TH1* histo = it->second;
        histo->Write();
    }
    fileOut->Close();
    // Saving canvas to an image
    fileName = this->fileNameBase + append + ".pdf";
    printf("Saving canvas with histograms to image: %s\n", fileName.c_str());
    this->canvas->Print(fileName.c_str());
    this->last_saved = time(NULL);
}

void Monitor::setStyle() {
    gStyle->SetHistMinimumZero(kTRUE);
    gStyle->SetOptStat(100110);
    gStyle->SetHistLineColor(COLORS[0]);
    gStyle->SetHistFillColor(FILL_COLORS[0]);
    gStyle->SetPadRightMargin(0.04);
    gStyle->SetTitleOffset(1.8, "Y");
    gStyle->SetPadLeftMargin(0.12);
}

void Monitor::readData(const TD_event &event, const TD_board &lc2228a, const TD_board &lc2249a, const TD_board &lc2249w, 
                       const TD_board &v259n, const TD_board &c257, const TD_board &lc2551, 
                       const TD_board &v560n, const TD_board &v560n_2) 
{
    bzero(&this->event, sizeof(this->event));
    // Reading data from event and each board
    this->event.dataErrMask = event.dataErrMask;
    // TDC
    size_t data_size = 2;
    for (size_t ch=0; ch<8; ++ch) {
        memcpy(&this->event.data_tdc[ch], &lc2228a.dataPld[ch*data_size], data_size);
        this->event.data_tdc[ch] = be16toh(this->event.data_tdc[ch]);
    }
    // ADC 2249a
    data_size = 2;
    for (size_t ch=0; ch<12; ++ch) {
        memcpy(&this->event.data_adc_lc2249a[ch], &lc2249a.dataPld[ch*data_size], data_size);
        this->event.data_adc_lc2249a[ch] = be16toh(this->event.data_adc_lc2249a[ch]);
    }
    // ADC 2249w
    data_size = 2;
    for (size_t ch=0; ch<12; ++ch) {
        memcpy(&this->event.data_adc_lc2249w[ch], &lc2249w.dataPld[ch*data_size], data_size);
        this->event.data_adc_lc2249w[ch] = be16toh(this->event.data_adc_lc2249w[ch]);
    }
    // Pattern unit
    data_size = 2;
    for (size_t ch=0; ch<2; ++ch) {
        memcpy(&this->event.data_pattern_unit[ch], &v259n.dataPld[ch*data_size], data_size);
        this->event.data_pattern_unit[ch] = be16toh(this->event.data_pattern_unit[ch]);
    }
    // Scaler c257
    data_size = 4;
    for (size_t ch=0; ch<16; ++ch) {
        memcpy(&this->event.data_scaler_c257[ch], &c257.dataPld[ch*data_size], data_size);
        this->event.data_scaler_c257[ch] = be32toh(this->event.data_scaler_c257[ch]);
    }
    // Scaler v560n
    data_size = 4;
    for (size_t ch=0; ch<16; ++ch) {
        memcpy(&this->event.data_scaler_v560n[ch], &v560n.dataPld[ch*data_size], data_size);
        this->event.data_scaler_v560n[ch] = be32toh(this->event.data_scaler_v560n[ch]);
    }
    // Scaler v560n II
    data_size = 4;
    for (size_t ch=0; ch<16; ++ch) {
        memcpy(&this->event.data_scaler_v560n2[ch], &v560n_2.dataPld[ch*data_size], data_size);
        this->event.data_scaler_v560n2[ch] = be32toh(this->event.data_scaler_v560n2[ch]);
    }
    // Filling histograms with the collected data
    this->fillEvent();
}
