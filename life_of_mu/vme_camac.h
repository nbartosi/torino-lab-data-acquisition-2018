/*
	This library allows to read or write 16bit or 24bit data to a CAMAC address via a VME bridge
*/

#ifndef VME_CAMAC_H
#define VME_CAMAC_H

#include <stdint.h>

#include <CAENVMElib.h>
//#include <CAENComm.h>

// Base address used for addressing of CAMAC modules trough VME CAMAC branch driver [CES CBD 8210]
// corresponds to the specific positions and configurations of modules in the setup
#define VME_CAMAC_BASE_ADDRESS 0x800000

// Defining constants for initialisation of connection to CAMAC through VME
#define VME_CAMAC_LINK 0   // irrelevant for V1718
#define VME_CAMAC_DEVICE 0   // slot number where the V1718 is installed
#define VME_CAMAC_BOARD cvV1718   // type of the VME interface board [defined in CAENVMEtypes.h]

// new enum type for selecting if read 16/24 or write 16/24 bits
typedef enum {  R16,          // read 16 bits
                R24,          // read 24 bits (low and high D16 word)
                W16,          // write 16 bits
                W24           // write 24 bits (low and high D16 word)
              } TD_command;

// Creates a handle to be used in further communication with CAMAC modules
CVErrorCodes VME_CAMAC_init(int32_t *handle);

// Sends a read or write command to CAMAC address defined using N A F values
CVErrorCodes VME_CAMAC_command(int32_t handle,
                                     uint8_t CR,
                                     uint8_t N,
                                     uint8_t A,
                                     uint8_t F,
                                     TD_command command,
                                     uint32_t *data);

// Maps error codes from CAEN VME library to error codes in CAEN Common library
//CAENComm_ErrorCode VME_to_Comm_error(CVErrorCodes error_vme);


#endif // VME_CAMAC_H
