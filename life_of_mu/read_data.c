#include <strings.h>
#include <stdio.h>
#include <string.h>
#include <endian.h>
#include <stdint.h>
#include <errno.h>

// #define NUM_INSTR 2           // number of board
// #define INSTRDESCRSIZE 64     // size of instrument description

extern int errno;

typedef struct {
  uint16_t eventSize;                           // 2 Bytes
  uint32_t eventNumber;                         // 4 Bytes
  uint16_t statusQ;                             // 2 Bytes
  // char[NUM_INSTR][INSTRDESCRSIZE] description;  // table with intrument descriptions
  // uint8_t dataPld[1000];
  uint8_t padding1[8];                           // 8 Bytes padding
  uint8_t description1[64];                      // 64 Bytes instrument 1
  uint8_t description2[64];                      // 64 Bytes instrument 2
  uint8_t description3[64];                      // 64 Bytes instrument 3
  uint8_t description4[64];                      // 64 Bytes instrument 4
  uint8_t description5[64];                      // 64 Bytes instrument 5
  uint8_t description6[64];                      // 64 Bytes instrument 6
  uint8_t description7[64];                      // 64 Bytes instrument 7
  uint8_t description8[64];                      // 64 Bytes instrument 8
  uint32_t data1[16];                            // 64 Bytes data instrument 1
  uint32_t data2[12];                            // 48 Bytes data instrument 2
  uint32_t data3[16];                            // 64 Bytes data instrument 3
  uint32_t data4[16];                            // 64 Bytes data instrument 4
  uint16_t data5[8];                             // 16 Bytes data instrument 5
  uint16_t data6[16];                            // 32 Bytes data instrument 6
  uint16_t data7[8];                             // 16 Bytes data instrument 7
  uint8_t data8[16];                             // 16 Bytes data instrument 8
  uint8_t padding2[16];                          // 16 Bytes padding
} TD_event;                                    // total: 856  bytes


int main(int argc, char** argv) {

  int errnum;

  TD_event myEvent;
  bzero(&myEvent, sizeof(TD_event));

  FILE *pmyDataFile;         // pointer to the data file

  printf("opening %s: ...", argv[1]);

  pmyDataFile = fopen(argv[1], "r");

  if (pmyDataFile == 0) {

      errnum = errno;
      fprintf(stderr, "Value of errno: %d\n", errno);
      perror("Error printed by perror");
      fprintf(stderr, "Error opening file: %s\n", strerror( errnum ));

      return errno;
   }

   printf("done!\n\n");

   size_t read = 0;
   size_t readTot = 0;


   while (1) {

     bzero(&myEvent, sizeof(TD_event));

     read = 0;
     readTot = 0;

     read = fread(&myEvent.eventSize, sizeof(uint8_t), 2, pmyDataFile);
     myEvent.eventSize = be16toh(myEvent.eventSize);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     if (feof(pmyDataFile)) {
       break;
     }

     read = fread(&myEvent.eventNumber, sizeof(uint8_t), 4, pmyDataFile);
     myEvent.eventNumber = be32toh(myEvent.eventNumber);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.statusQ, sizeof(uint8_t), 2, pmyDataFile);
     myEvent.statusQ = be16toh(myEvent.statusQ);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.padding1, sizeof(uint8_t), 8, pmyDataFile);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.description1, sizeof(uint8_t), 64, pmyDataFile);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.description2, sizeof(uint8_t), 64, pmyDataFile);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.description3, sizeof(uint8_t), 64, pmyDataFile);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.description4, sizeof(uint8_t), 64, pmyDataFile);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.description5, sizeof(uint8_t), 64, pmyDataFile);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.description6, sizeof(uint8_t), 64, pmyDataFile);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.description7, sizeof(uint8_t), 64, pmyDataFile);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.description8, sizeof(uint8_t), 64, pmyDataFile);
     readTot += read;
//   printf("read: %d,  rearTot: %x\n", read, readTot);


//     read = fread(&myEvent.data1, sizeof(uint32_t), 16, pmyDataFile);
     read = fread(&myEvent.data1, sizeof(uint32_t), 16, pmyDataFile);
     read = (read * (sizeof(uint32_t)));
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.data2, sizeof(uint32_t), 12, pmyDataFile);
     read = (read * (sizeof(uint8_t)));
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.data3, sizeof(uint32_t), 16, pmyDataFile);
     read = (read * (sizeof(uint32_t)));
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.data4, sizeof(uint32_t), 16, pmyDataFile);
     read = (read * (sizeof(uint32_t)));
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.data5, sizeof(uint16_t), 8, pmyDataFile);
     read = (read * (sizeof(uint16_t)));
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.data6, sizeof(uint16_t), 16, pmyDataFile);
     read = (read * (sizeof(uint16_t)));
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.data7, sizeof(uint16_t), 8, pmyDataFile);
     read = (read * (sizeof(uint16_t)));
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.data8, sizeof(uint8_t), 16, pmyDataFile);
     read = (read * (sizeof(uint8_t)));
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     read = fread(&myEvent.padding2, sizeof(uint8_t), 16, pmyDataFile);
     readTot += read;
//     printf("read: %d,  rearTot: %x\n", read, readTot);

     printf("\n\n= = = => Read bytes: %x\n", uint8_t(readTot));

     printf("eventSize: 0x%x, eventNumber: %d, statusQ: %.4x, padding1: %.8s\n\n"
             "description1: %.64s\ndescription2: %.64s\ndescription3: %.64s\n"
             "description4: %.64s\ndescription5: %.64s\ndescription6: %.64s\ndescription7: %.64s\ndescription8: %.64s\n\ndata1: ",
             myEvent.eventSize, myEvent.eventNumber, myEvent.statusQ, myEvent.padding1,
             myEvent.description1, myEvent.description2, myEvent.description3,
             myEvent.description4, myEvent.description5, myEvent.description6, myEvent.description7, myEvent.description8);




    int i = 0;
    char hexOpt[10] = "-hex";
    if (argv[2] != NULL) {

    printf("\nstring: %s, argv2: %s\n\n", hexOpt, argv[2]);

      if ((strcmp(argv[2], hexOpt)) == 0) {
        for (i = 0; i < 16; i++) {
          printf("ch%d_%.8x ", i, be32toh(myEvent.data1[i]));
        }
        printf("\ndata2: ");
        for (i = 0; i < 12; i++) {
          printf("ch%d_%.8x ", i, be32toh(myEvent.data2[i]));
        }
        printf("\ndata3: ");
        for (i = 0; i < 16; i++) {
          printf("ch%d_%.8x ", i, be32toh(myEvent.data3[i]));
        }
        printf("\ndata4: ");
        for (i = 0; i < 16; i++) {
          printf("ch%d_%.8x ", i, be32toh(myEvent.data4[i]));
        }
        printf("\ndata5: ");
        for (i = 0; i < 8; i++) {
          printf("ch%d_%.4x ", i, be16toh(myEvent.data5[i]));
        }
        printf("\ndata6: ");
        for (i = 0; i < 12; i++) {
          printf("ch%d_%.4x ", i, be16toh(myEvent.data6[i]));
        }
        printf("\ndata7: ");
        for (i = 0; i < 2; i++) {
          printf("ch%d_%.8x ", i, be16toh(myEvent.data7[i]));
        }
        printf("\ndata8: ");
        for (i = 0; i < 16; i++) {
          printf("ch%d_%.8x ", i, myEvent.data8[i]);
        }
        printf("\npadding2: %.16s\n\n\n", myEvent.padding2);
      }
      else {
        printf("wrong argument\n");
        return 0;
      }
    }
    else {

      for (i = 0; i < 16; i++) {
        printf("ch%d_%d ", i, be32toh(myEvent.data1[i]));
      }
      printf("\ndata2: ");
      for (i = 0; i < 12; i++) {
        printf("ch%d_%d ", i, be32toh(myEvent.data2[i]));
      }
      printf("\ndata3: ");
      for (i = 0; i < 16; i++) {
        printf("ch%d_%d  ", i, be32toh(myEvent.data3[i]));
      }
      printf("\ndata4: ");
      for (i = 0; i < 16; i++) {
        printf("ch%d_%d ", i, be32toh(myEvent.data4[i]));
      }
      printf("\ndata5: ");
      for (i = 0; i < 8; i++) {
        printf("ch%d_%d ", i, be16toh(myEvent.data5[i]));
      }
      printf("\ndata6: ");
      for (i = 0; i < 12; i++) {
        printf("ch%d_%d ", i, be16toh(myEvent.data6[i]));
      }
      printf("\ndata7: ");
      for (i = 0; i < 2; i++) {
        printf("ch%d_%d ", i, be16toh(myEvent.data7[i]));
      }
      printf("\ndata8: ");
      for (i = 0; i < 16; i++) {
        printf("ch%d_%d ", i, myEvent.data8[i]);
      }
      printf("\npadding2: %.16s\n\n\n", myEvent.padding2);
    }

  }




  //fprintf(pfile, "This is testing for fprintf...\n");
  //fputs("This is testing for fputs...\n", pfile);
  fclose(pmyDataFile);

  return 0;
}
