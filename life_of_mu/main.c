

#include <strings.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <endian.h>
#include <time.h>
#include <errno.h>

#include <unistd.h>  // For debugging the progressive filling of histograms

#include "./main.h"
#include "./vme_camac.h"
#include "./monitor.h"

#include <CAENVMElib.h>


extern int errno;
volatile sig_atomic_t sigint_received = 0;

void signal_handler(int s) {
    sigint_received = 1;
}

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 *             M  A  I  N
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 */

int main(int argc, char** argv) {

  int errnum;       // error code (error.h)

  // registering handler for SIGINT signal to normally stop program execution
  signal(SIGINT, signal_handler);

 /*
  *    B O A R D     D E F I N I T I O N
  *
  * define here the parameter of each board (camac and VME).
  *
  * ==> comment the unused (or unwanted) boards


  // generic board definition schema
  TD_board myBoard;                       // declaration
  bzero(&myBoard, sizeof(TD_board));      // inizialization
  strcpy(myBoard.description, "myBoard description. INSTRDESCRSIZE -1 is max size");
  myBoard.baseAddr = 0x00112233;          // VME: base address  CAMAC: N number of board slot
  myBoard.dataSize = 64;                  // size of read data in Bytes. (es. 16channel x 32bit)
  myBoard.addressModifier =               // address modifier (provided by board documentation)

  */


  // CAEN C257 - scaler
  TD_board c257;                             // declare board_definition
  bzero(&c257, sizeof(TD_board));            // initialize
  c257.type = camac;
  c257.statusQ_index = 0;
  strcpy(c257.description, "C257 - scaler - 16ch x 32 (24)bit");   // description
  // c257.baseAddr = 10;                      // ====> UPDATE N ADDRESS <=====
  c257.baseAddr = 9;
  c257.dataSize = 16 * (sizeof(uint32_t)); // 16ch x 32bit/each = 512 bits = 64 Bytes

  // LeCroy 2551 - scaler
  TD_board lc2551;                           // declare board_definition
  bzero(&lc2551, sizeof(TD_board));          // initialize
  lc2551.type = camac;
  lc2551.statusQ_index = 1;
  strcpy(lc2551.description, "LeCroy 2251 - scaler - 12ch x 32 (24)bit");   // description
  lc2551.baseAddr = 8;                       // ====> UPDATE N ADDRESS <=====
  lc2551.dataSize = 12 * (sizeof(uint32_t)); // 12ch x 32bit/each = 384 bits =  48 Bytes

  // CAEN V560N - scaler
  TD_board v560n;                            // declare board
  bzero(&v560n, sizeof(TD_board));           // initialize
  v560n.type = vme;
  v560n.statusQ_index = 2;
  strcpy(v560n.description, "V560N - scaler - 16ch x 32bit");   // description
  // v560n.baseAddr = 0x00222300;            // address: 0x00222300
  v560n.baseAddr = 0x00222300;               // address: 0x00222400
  v560n.dataSize = 16 * (sizeof(uint32_t));  // 16ch x 32bit/each = 512 bits = 64 Bytes
  v560n.addressModifier = cvA32_U_DATA;      // read from the manual

  // CAEN V560N - II scaler
  TD_board v560n_2;                            // declare board
  bzero(&v560n_2, sizeof(TD_board));           // initialize
  v560n_2.type = vme;
  v560n_2.statusQ_index = 3;
  strcpy(v560n_2.description, "V560N - scaler inhibited - 16ch x 32bit");   // description
  // v560n.baseAddr = 0x00222300;            // address: 0x00222300
  v560n_2.baseAddr = 0x00222400;               // address: 0x00222400
  v560n_2.dataSize = 16 * (sizeof(uint32_t));  // 16ch x 32bit/each = 512 bits = 64 Bytes
  v560n_2.addressModifier = cvA32_U_DATA;      // read from the manual

  // LECROY 2228A - TDC
  TD_board lc2228a;                          // declare board_definition
  bzero(&lc2228a, sizeof(TD_board));         // initialize
  lc2228a.type = camac;
  lc2228a.statusQ_index = 4;
  strcpy(lc2228a.description, "2228A - tdc - 8ch x 16 (11)bit");   // description
  lc2228a.baseAddr = 17;                     // ====> UPDATE N ADDRESS <=====
  lc2228a.dataSize = 8 * (sizeof(uint16_t)); // 8ch x 16bit/each = 128 bits = 16 Bytes

  // LECROY 2249A - ADC
  TD_board lc2249a;                          // declare board_definition
  bzero(&lc2249a, sizeof(TD_board));         // initialize
  lc2249a.type = camac;
  lc2249a.statusQ_index = 5;
  strcpy(lc2249a.description, "2249A - adc - 12ch x 16 (10)bit + 8 Bytes padding");   // description
  lc2249a.baseAddr = 2;                 // ====> UPDATE N ADDRESS <=====
  lc2249a.dataSize = (12 * (sizeof(uint16_t))) + 8; // 12ch x 16bit/each = 192 bits = 24 Bytes + 8 Bytes padding

  // LECROY 2249W - ADC
  TD_board lc2249w;                          // declare board_definition
  bzero(&lc2249w, sizeof(TD_board));         // initialize
  lc2249w.type = camac;
  lc2249w.statusQ_index = 6;
  strcpy(lc2249w.description, "2249W - adc - 12ch x 16 (11)bit + 8 Bytes padding");   // description
  lc2249w.baseAddr = 3;                 // ====> UPDATE N ADDRESS <=====
  lc2249w.dataSize = (12 * (sizeof(uint16_t))) + 8; // 12ch x 16bit/each = 192 bits = 24 Bytes + 8 Bytes padding

  // CAEN V259N - pattern unit
  TD_board v259n;                            // declare board
  bzero(&v259n, sizeof(TD_board));           // initialize
  v259n.type = vme;
  v259n.statusQ_index = 7;
  strcpy(v259n.description, "V259N - multi-hit patter unit - 16 bit pattern reg. + 16 bit mult. reg. + 12 Bytes padding");   // description
  v259n.baseAddr = 0x00444100;               // ====> UPDADE B_ADDRESS <====
  v259n.dataSize = (2 * (sizeof(uint16_t)))+ 12;   // 16 bit + 16 bit = 4 Bytes
  v259n.addressModifier = cvA24_U_DATA;      // read from the manual

  // CAEN C211 - programmable delay
  TD_board c211;                             // declare board_definition
  bzero(&c211, sizeof(TD_board));            // initialize
  c211.type = camac;
  c211.statusQ_index = 8;
  strcpy(c211.description, "C211 - programmable delay - 16ch x 8bit");   // description
  c211.baseAddr = 5;                        // ====> UPDADE B_ADDRESS <====
  c211.dataSize = 16 * (sizeof(uint8_t));    // 16ch x 8bit/each = 128 bits = 16 Bytes


  // do not modify the following module
  // CES RCB8047 - corbo
  TD_board corbo;                      // declare board
  bzero(&corbo, sizeof(TD_board));     // initialize
  corbo.type = vme;
  strcpy(corbo.description, "CORBO");     // description
  corbo.baseAddr = 0x00BBBB00;            // corbo: 0x00BBBB00
  corbo.addressModifier = cvA24_U_DATA;   // read from the manual


  // structure to store the current event
  TD_event event;
  bzero(&event, sizeof(TD_event));
  event.pdescription = &event.description[0];
  event.pdata = &event.data[0];

  /*
   * - create the directory ./data
   * - build string with filename from current date/time
   * - create a file for saving data
   */
  struct stat newDir = {0};
  if(stat("./data", &newDir) == -1) {   // check if ./data exists
    mkdir("./data", 0755);              // mkdir ./data
  }

  // get current time in secs from 1/1/1970
  time_t rawTime;
  time(&rawTime);

  // fill the structure time with the current time
  struct tm* timestamp;
  timestamp = localtime(&rawTime);

  // create the new filename
  char myFileName[100];
  bzero(myFileName, 100 * sizeof(char));
  int strOffset = 0;
  strOffset += sprintf(&myFileName[strOffset], "./data/");
  strOffset += strftime(&myFileName[strOffset], 100 - 4 - strOffset, "%Y%m%d_%H%M%S", timestamp);
  strOffset += sprintf(&myFileName[strOffset], ".dat");

  if(DBGMODE) {
    printf("file_name: %s\n", myFileName);
  }

  // Creating instance of Live Monitor
  Monitor *monitor = new Monitor(myFileName);

  // =====> CREATE THE NEW FILE <=====
  FILE *pmyFile;                    // pointer to file
  pmyFile = fopen(myFileName, "a");

  if (pmyFile == NULL) {
      errnum = errno;
      fprintf(stderr, "Value of errno: %d\n", errno);
      perror("Error printed by perror");
      fprintf(stderr, "Error opening file: %s\n", strerror( errnum ));

      return errno;
   }


  // "tools" variables
  uint32_t dataBuffer = 0;            // variable for storing r/w from/to vme V1718
  uint32_t fullAddr = 0;
  uint16_t dataBuffer16 = 0;
  uint8_t triggered = 0;
  uint8_t CR = 1;                     // crate number. As there is only one crate, I declared it here.
  uint8_t N = 0;
  uint8_t A = 0;
  uint8_t F = 0;
  uint32_t eventCounter = 0;
  uint16_t eventSize = 0;
  size_t byteWrote = 0;               // bytes wrote on file
  uint8_t dummyVar = 0;



  /*
   *  OPEN USB CONNECTION TO V1718
   */

  // returned value for CAENVMElib's functions
  CVErrorCodes myError = CVErrorCodes::cvSuccess;

  // stuff needed for initializing the V1718 bridge
  CVBoardTypes BdType = cvV1718;
  short Link = 0;
  short BdNum = 0;
  int32_t Handle;

  // try to connect to the V1718
  myError = CAENVME_Init(BdType, Link, BdNum, &Handle);

  // if doesn't connect, return error and quit
  if (myError != cvSuccess) {
      decodeCaenMyError(&myError);
      return(myError);
  }


  /* * * * * * * * * * * * * *
   * MODULES INITIALIZATION  *
   * * * * * * * * * * * * * */

  /*
   * send signal Z (initialization) to the camac crate CR
   * CBD 8210: commands N28 A8 F26
   */
  printf("Initialization (CAMAC Z) of the camac crate CR%d ...", CR);
  camacZ(Handle, CR);
  printf(" + done!\n");

  /*
   * send signal C (clear) to the camac create CR
   * CBD 8210: commands N28 A9 F26
   */
  printf("Clear modules (CAMAC C) of the camac crate CR%d ...", CR);
  camacC(Handle, CR);
  printf(" + done!\n");


  // clear counters of v560n
  fullAddr = (v560n.baseAddr + 0x50);
  myError = CAENVME_WriteCycle(Handle, fullAddr, &dataBuffer, v560n.addressModifier, cvD16);
  if (myError != cvSuccess) {
    decodeCaenMyError(&myError);
  }

  // clear counters of v560n - II scaler
  fullAddr = (v560n_2.baseAddr + 0x50);
  myError = CAENVME_WriteCycle(Handle, fullAddr, &dataBuffer, v560n_2.addressModifier, cvD16);
  if (myError != cvSuccess) {
    decodeCaenMyError(&myError);
  }

  // clear counters of v259n
  fullAddr = (v259n.baseAddr + 0x20);
  myError = CAENVME_WriteCycle(Handle, fullAddr, &dataBuffer, v259n.addressModifier, cvD16);
  if (myError != cvSuccess) {
    decodeCaenMyError(&myError);
  }


  /*
   * configure programmable delay CAEN c211
   */
  // dummy delay's initialization
  // ch0, ch1, ch2, ..., ch15
  if(DBGMODE) {
   printf("Configuring programmable delay Caen c211 ...");
  }
  uint8_t delayArray[16] = {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7,
                            0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF};

  N = (uint8_t)c211.baseAddr;
  F = 16;
  c211.statusQ = 1;                 // initialize statusQ to default value before start writing channels
  for(A=0; A<16; A++) {
    dataBuffer = (uint32_t)delayArray[A];
    myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, W16, &dataBuffer);
    if (myError != cvSuccess) {
       decodeCaenMyError(&myError);
    }

    // // change uint32_t from architecture to Big Endian
    // dataBuffer = htobe16(dataBuffer);

    // fill the dataPld array with the read datas with a 4 Bytes bunch
    dummyVar = (uint8_t)dataBuffer;
    memcpy(&c211.dataPld[A], &dummyVar, sizeof(dummyVar));

    // check statusQ of each camac cycle AND the previous status.
    // 1 is default value; in case of Q=0 ===> Qvalue = 0 and the
    // value of all read channels is not confirmed (global statusQ = 0)
    c211.statusQ &= statusQ(Handle);
  }


  // activate the delays on all channels: CRx, Nx, A0, F19
  A = 0;
  F = 19;
  myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, W16, &dataBuffer);
  if (myError != cvSuccess) {
     decodeCaenMyError(&myError);
  }

  c211.statusQ &= statusQ(Handle);

  // put statusQ in event dataErrMask (uint16_t)
  event.dataErrMask |= ((uint16_t)c211.statusQ << (15 - c211.statusQ_index));

  if(DBGMODE) {
    printf(" + done!\n");
  }


  /*
   * remove camac dataway inhibit (I) from the camac busy
   * CBD 8210: commands N30 A9 F24
   */
  printf("Clear inhibit (I) from camac bus ...");
  inhibit(Handle, CR, unsetINH);
  printf(" + done!\n");


  /*
   * ===> corbo <===
   * 1) write [D16] 0x0060 on 0x00, 0x02, 0x04, 0x06
   * 2) write [D8] 0x00 on 0x31, 0x33, 0x35, 0x37
   */

   // ===> 1)
   printf("Initializing corbo ...");
   // dataBuffer = 0x0060;
   dataBuffer = 0x00C0;

   fullAddr = (corbo.baseAddr + 0x00);
   myError = CAENVME_WriteCycle(Handle, fullAddr, &dataBuffer, cvA24_U_DATA, cvD16);
   if (myError != cvSuccess) {
       decodeCaenMyError(&myError);
   }

   fullAddr = (corbo.baseAddr + 0x02);
   myError = CAENVME_WriteCycle(Handle, fullAddr, &dataBuffer, cvA24_U_DATA, cvD16);
   if (myError != cvSuccess) {
       decodeCaenMyError(&myError);
   }

   fullAddr = (corbo.baseAddr + 0x04);
   myError = CAENVME_WriteCycle(Handle, fullAddr, &dataBuffer, cvA24_U_DATA, cvD16);
   if (myError != cvSuccess) {
       decodeCaenMyError(&myError);
   }

   fullAddr = (corbo.baseAddr + 0x06);
   myError = CAENVME_WriteCycle(Handle, fullAddr, &dataBuffer, cvA24_U_DATA, cvD16);
   if (myError != cvSuccess) {
       decodeCaenMyError(&myError);
   }


   // ===> 2)
   dataBuffer = 0x00;

   fullAddr = (corbo.baseAddr + 0x31);
   myError = CAENVME_WriteCycle(Handle, fullAddr, &dataBuffer, cvA24_U_DATA, cvD8);
   if (myError != cvSuccess) {
       decodeCaenMyError(&myError);
   }

   fullAddr = (corbo.baseAddr + 0x33);
   myError = CAENVME_WriteCycle(Handle, fullAddr, &dataBuffer, cvA24_U_DATA, cvD8);
   if (myError != cvSuccess) {
       decodeCaenMyError(&myError);
   }

   fullAddr = (corbo.baseAddr + 0x35);
   myError = CAENVME_WriteCycle(Handle, fullAddr, &dataBuffer, cvA24_U_DATA, cvD8);
   if (myError != cvSuccess) {
       decodeCaenMyError(&myError);
   }

   fullAddr = (corbo.baseAddr + 0x37);
   myError = CAENVME_WriteCycle(Handle, fullAddr, &dataBuffer, cvA24_U_DATA, cvD8);
   if (myError != cvSuccess) {
       decodeCaenMyError(&myError);
   }
   printf(" + done!\n");



   /*
    * polling corbo and waiting for a trigger until a key is pressed
    *
    * 1) read [D16] on 0x00, 0x02, 0x04, 0x06
    * 2) check the bit<09> : if true there is a trigger
    */

    // clear del corbo
    printf(" clear busy ...");
    clearCorbo(Handle, &corbo);
    triggered = 0;
    printf(" + done!\n\n");

    // Timestamp to track time of each event
    char timestamp_string[50];
    
    // Infinite loop until ENTER is pressed 
    while(1) {
      // checking for SIGINT signal
      if (sigint_received == 1) break;

      triggered = pollingCorbo(Handle, &corbo);
      // triggered = 1;

      if(DBGMODE) {
        printf("trigger with corbo\n");
      }

      /* * * * * * * * * * * * * * * * * * * * * *
       * Doing nothing if no event was triggered *
       * * * * * * * * * * * * * * * * * * * * * */
      if(triggered < 1) {
        continue;
      }

      /* * * * * * * * * * * * * * * * * * *
       *          EVENT TRIGGERED          *
       * --------------------------------- *
       * Processing data from each module  *
       * * * * * * * * * * * * * * * * * * */

      // building the timestamp string
      rawTime = time(NULL);
      timestamp = localtime(&rawTime);
      bzero(&timestamp_string[0], sizeof(timestamp_string));
      strftime(&timestamp_string[0], sizeof(timestamp_string), "%d/%m/%Y %H:%M:%S", timestamp);

      // increment the event counter
      eventCounter += 1;
      printf("### %s eventCounter: %d\n", timestamp_string, eventCounter);

      if(DBGMODE) {
        printf(" corbo has been triggered. Event number: %u\n", eventCounter);
      }

      // set Inhibit on camac
      inhibit(Handle, CR, setINH);


      /*
       *         read scaler CAEN c257
       */
      if(DBGMODE) {
        printf("Start reading scaler Caen c257 ...");
      }

      N = (uint8_t)c257.baseAddr;
      F = 2;
      c257.statusQ = 1;                 // initialize statusQ to default value before start reading channels
      // for(A=0; A<16; A++) {
      for(A=15; A<16; A++) {
        myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, R24, &dataBuffer);
        if (myError != cvSuccess) {
            decodeCaenMyError(&myError);
        }
        if(DBGMODE && A == 0) {
          printf(" c257 ch 0: 0x%x\tstatusQ: %d\n", dataBuffer, statusQ(Handle));
        }

        /*
         * mind the endianness: Intel is a little endian architecture.
         *
         * ex.
         * uint32_t myVar = 0x12345678;
         * uint8_t myArray[4];
         *
         * memcpy(&myArray[0], &myVar, sizeof(uint32_t));
         *
         * int i = 0;
         * printf("myVar: 0x%x\n", myVar1);
         * while (i < 4) {
         *  printf("myArray[%d]: 0x%x\n", i, myArray[i]);
         *  i++;
         * }
         *
         * the output will be:
         * myVar: 0x12345678
         * myArray[0]: 0x78
         * myArray[1]: 0x56
         * myArray[2]: 0x34
         * myArray[3]: 0x12
         *
         * this means that the bytes I copied from myVar where stored
         * in reverse order (lowest Byte -0x78- at left and highest Byte -0x12- at right).
         * This order is called "little endian".
         *
         * What we need is storing events in a "big endian" order.
         * see: man -s3 endian
         */

        // change uint32_t from architecture to Big Endian
        dataBuffer = htobe32(dataBuffer);

        // fill the dataPld array with the read datas with a 4 Bytes bunch
        memcpy(&c257.dataPld[A*4], &dataBuffer, sizeof(dataBuffer));

        // check statusQ of each camac cycle AND the previous status.
        // 1 is default value; in case of Q=0 ===> Qvalue = 0 and the
        // value of all read channels is not confirmed (global statusQ = 0)
        c257.statusQ &= statusQ(Handle);
      }

      // put statusQ in event dataErrMask (uint16_t)
      event.dataErrMask |= ((uint16_t)c257.statusQ << (15 - c257.statusQ_index));

      if(DBGMODE) {
        printf(" + done!\n");
      }

      if(DBGMODE) {
        printf("Moving c257's data ...");
      }
      // fill the event structure with the description and the data
      memcpy(event.pdescription, &c257.description, sizeof(TD_instrDescr));
      event.pdescription += sizeof(TD_instrDescr);                // first free element of event.description
      event.descrSize += sizeof(TD_instrDescr);
      memcpy(event.pdata, &c257.dataPld, c257.dataSize * sizeof(uint8_t));
      event.pdata += (c257.dataSize * sizeof(uint8_t));           // first free element of event.data
      event.dataSize += (c257.dataSize * sizeof(uint8_t));

      if(DBGMODE) {
        printf(" + done!\n");
      }


      /* #######################################
       *         read scaler LECROY lc2551
       * #######################################
       */
      if(DBGMODE) {
       printf("Start reading scaler LeCroy 2551 ...");
      }

      N = (uint8_t)lc2551.baseAddr;
      F = 2;
      lc2551.statusQ = 1;                 // initialize statusQ to default value before start reading channels
      // for(A=0; A<12; A++) {
      for(A=0; A<0; A++) {
        myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, R24, &dataBuffer);
        if (myError != cvSuccess) {
            decodeCaenMyError(&myError);
        }

        // change uint32_t from architecture to Big Endian
        dataBuffer = htobe32(dataBuffer);

        // fill the dataPld array with the read datas with a 4 Bytes bunch
        memcpy(&lc2551.dataPld[A*4], &dataBuffer, sizeof(dataBuffer));



        // check statusQ of each camac cycle AND the previous status.
        // 1 is default value; in case of Q=0 ===> Qvalue = 0 and the
        // value of all read channels is not confirmed (global statusQ = 0)
        lc2551.statusQ &= statusQ(Handle);
      }

      // put statusQ in event dataErrMask (uint16_t)
      event.dataErrMask |= ((uint16_t)lc2551.statusQ << (15 - lc2551.statusQ_index));

      if(DBGMODE) {
        printf(" + done!\n");
      }

      if(DBGMODE) {
        printf("Moving lc2551's data ...");
      }
      // fill the event structure with the description and the data
      memcpy(event.pdescription, &lc2551.description, sizeof(TD_instrDescr));
      event.pdescription += sizeof(TD_instrDescr);                // first free element of event.description
      event.descrSize += sizeof(TD_instrDescr);
      memcpy(event.pdata, &lc2551.dataPld, lc2551.dataSize * sizeof(uint8_t));
      event.pdata += (lc2551.dataSize * sizeof(uint8_t));           // first free element of event.data
      event.dataSize += (lc2551.dataSize * sizeof(uint8_t));

      if(DBGMODE) {
        printf(" + done!\n");
      }


      /* #######################################
       *         read scaler CAEN v560n
       * #######################################
       */
       if(DBGMODE) {
         printf("Start reading v560n ...");
       }

       v560n.statusQ = 1;

       // for(A=0; A<16; A++) {
       for(A=0; A<1; A++) {
         fullAddr = (v560n.baseAddr + 0x10 + (A * 0x4));
         myError = CAENVME_ReadCycle(Handle, fullAddr, &dataBuffer, v560n.addressModifier, cvD32);
         if (myError != cvSuccess) {
           decodeCaenMyError(&myError);
           v560n.statusQ = 0;
         }
         if(DBGMODE && A == 0) {
           printf(" v560 ch 0: 0x%x\n", dataBuffer);
         }

         // change uint32_t from architecture to Big Endian
         dataBuffer = htobe32(dataBuffer);

         // fill the dataPld array with the read datas with a 4 Bytes bunch
         memcpy(&v560n.dataPld[A*4], &dataBuffer, sizeof(dataBuffer));
       }

       // put statusQ in event dataErrMask (uint16_t)
       event.dataErrMask |= ((uint16_t)v560n.statusQ << (15 - v560n.statusQ_index));

       if(DBGMODE) {
         printf(" + done!\n");
       }

       if(DBGMODE) {
         printf("Moving v560n's data ...");
       }
       // fill the event structure with the description and the data
       memcpy(event.pdescription, &v560n.description, sizeof(TD_instrDescr));
       event.pdescription += sizeof(TD_instrDescr);                // first free element of event.description
       event.descrSize += sizeof(TD_instrDescr);
       memcpy(event.pdata, &v560n.dataPld, v560n.dataSize * sizeof(uint8_t));
       event.pdata += (v560n.dataSize * sizeof(uint8_t));           // first free element of event.data
       event.dataSize += (v560n.dataSize * sizeof(uint8_t));

       // clear counters
       // fullAddr = (v560n.baseAddr + 0x50);
       // myError = CAENVME_ReadCycle(Handle, fullAddr, &dataBuffer, v560n.addressModifier, cvD16);
       // if (myError != cvSuccess) {
       //   decodeCaenMyError(&myError);
       // }

       if(DBGMODE) {
         printf(" + done!\n");
       }


       /* #######################################
        *         read scaler CAEN v560n - II
        * #######################################
        */
        if(DBGMODE) {
          printf("Start reading v560n II ...");
        }

        v560n_2.statusQ = 1;

        // for(A=0; A<16; A++) {
        for(A=0; A<1; A++) {
          fullAddr = (v560n_2.baseAddr + 0x10 + (A * 0x4));
          myError = CAENVME_ReadCycle(Handle, fullAddr, &dataBuffer, v560n_2.addressModifier, cvD32);
          if (myError != cvSuccess) {
            decodeCaenMyError(&myError);
            v560n_2.statusQ = 0;
          }
          if(DBGMODE && A == 0) {
            printf("v560 II ch 0: 0x%x\n", dataBuffer);
          }

          // change uint32_t from architecture to Big Endian
          dataBuffer = htobe32(dataBuffer);

          // fill the dataPld array with the read datas with a 4 Bytes bunch
          memcpy(&v560n_2.dataPld[A*4], &dataBuffer, sizeof(dataBuffer));
        }

        // put statusQ in event dataErrMask (uint16_t)
        event.dataErrMask |= ((uint16_t)v560n_2.statusQ << (15 - v560n_2.statusQ_index));

        if(DBGMODE) {
          printf(" + done!\n");
        }


        if(DBGMODE) {
          printf("Moving v560n's II data ...");
        }
        // fill the event structure with the description and the data
        memcpy(event.pdescription, &v560n_2.description, sizeof(TD_instrDescr));
        event.pdescription += sizeof(TD_instrDescr);                // first free element of event.description
        event.descrSize += sizeof(TD_instrDescr);
        memcpy(event.pdata, &v560n_2.dataPld, v560n_2.dataSize * sizeof(uint8_t));
        event.pdata += (v560n_2.dataSize * sizeof(uint8_t));           // first free element of event.data
        event.dataSize += (v560n_2.dataSize * sizeof(uint8_t));

        // clear counters
        // fullAddr = (v560n.baseAddr + 0x50);
        // myError = CAENVME_ReadCycle(Handle, fullAddr, &dataBuffer, v560n.addressModifier, cvD16);
        // if (myError != cvSuccess) {
        //   decodeCaenMyError(&myError);
        // }

        if(DBGMODE) {
          printf(" + done!\n");
        }


      /* #######################################
       *         read TDC LeCroy 2228A
       * #######################################
       */
      if(DBGMODE) {
        printf("Start reading TDC LeCroy 2228A ...");
      }

      N = (uint8_t)lc2228a.baseAddr;
      F = 2;
      lc2228a.statusQ = 1;                 // initialize statusQ to default value before start reading channels
      // for(A=0; A<8; A++) {
      for(A=7; A<8; A++) {
        myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, R16, &dataBuffer);
        if (myError != cvSuccess) {
            decodeCaenMyError(&myError);
        }

        // change uint32_t from architecture to Big Endian
        dataBuffer16 = htobe16((uint16_t)dataBuffer);

        // fill the dataPld array with the read datas with a 2 Bytes bunch
        memcpy(&lc2228a.dataPld[A*2], &dataBuffer16, sizeof(dataBuffer16));



        // check statusQ of each camac cycle AND the previous status.
        // 1 is default value; in case of Q=0 ===> Qvalue = 0 and the
        // value of all read channels is not confirmed (global statusQ = 0)
        lc2228a.statusQ &= statusQ(Handle);
      }

      // put statusQ in event dataErrMask (uint16_t)
      event.dataErrMask |= ((uint16_t)lc2228a.statusQ << (15 - lc2228a.statusQ_index));

      if(DBGMODE) {
        printf(" + done!\n");
      }

      if(DBGMODE) {
        printf("Moving lclc2228a's data ...");
      }
      // fill the event structure with the description and the data
      memcpy(event.pdescription, &lc2228a.description, sizeof(TD_instrDescr));
      event.pdescription += sizeof(TD_instrDescr);                // first free element of event.description
      event.descrSize += sizeof(TD_instrDescr);
      memcpy(event.pdata, &lc2228a.dataPld, lc2228a.dataSize * sizeof(uint8_t));
      event.pdata += (lc2228a.dataSize * sizeof(uint8_t));           // first free element of event.data
      event.dataSize += (lc2228a.dataSize * sizeof(uint8_t));

      if(DBGMODE) {
        printf(" + done!\n");
      }

      /* #######################################
       *         read ADC LeCroy 2249A
       * #######################################
       */
      if(DBGMODE) {
        printf("Start reading ADC LeCroy 2249A ...");
      }

      N = (uint8_t)lc2249a.baseAddr;
      F = 2;
      lc2249a.statusQ = 1;                 // initialize statusQ to default value before start reading channels
      // for(A=0; A<12; A++) {
      for(A=0; A<0; A++) {
        myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, R16, &dataBuffer);
        if (myError != cvSuccess) {
            decodeCaenMyError(&myError);
        }

        // change uint32_t from architecture to Big Endian
        dataBuffer16 = htobe16((uint16_t)dataBuffer);

        // fill the dataPld array with the read datas with a 2 Bytes bunch
        memcpy(&lc2249a.dataPld[A*2], &dataBuffer16, sizeof(dataBuffer16));


        // check statusQ of each camac cycle AND the previous status.
        // 1 is default value; in case of Q=0 ===> Qvalue = 0 and the
        // value of all read channels is not confirmed (global statusQ = 0)
        lc2249a.statusQ &= statusQ(Handle);
      }

      // put statusQ in event dataErrMask (uint16_t)
      event.dataErrMask |= ((uint16_t)lc2249a.statusQ << (15 - lc2249a.statusQ_index));

      // add 8 Bytes of padding: "AAAAAAAA"
      dataBuffer16 = 0x4141;        // ascii "AA" characters
      dataBuffer16 = htobe16(dataBuffer16);
      for(A=0; A<4; A++) {
        memcpy(&lc2249a.dataPld[24+(A*2)], &dataBuffer16, sizeof(dataBuffer16));
      }

      if(DBGMODE) {
        printf(" + done!\n");
      }

      if(DBGMODE) {
        printf("Moving lc2249a's data ...");
      }
      // fill the event structure with the description and the data
      memcpy(event.pdescription, &lc2249a.description, sizeof(TD_instrDescr));
      event.pdescription += sizeof(TD_instrDescr);                // first free element of event.description
      event.descrSize += sizeof(TD_instrDescr);
      memcpy(event.pdata, &lc2249a.dataPld, lc2249a.dataSize * sizeof(uint8_t));
      event.pdata += (lc2249a.dataSize * sizeof(uint8_t));           // first free element of event.data
      event.dataSize += (lc2249a.dataSize * sizeof(uint8_t));

      if(DBGMODE) {
        printf(" + done!\n");
      }


      /* #######################################
       *         read ADC LeCroy 2249W
       * #######################################
       */
      if(DBGMODE) {
        printf("Start reading ADC LeCroy 2249W ...");
      }

      N = (uint8_t)lc2249w.baseAddr;
      F = 2;
      lc2249w.statusQ = 1;                 // initialize statusQ to default value before start reading channels
      // for(A=0; A<12; A++) {
      for(A=10; A<12; A++) {
        myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, R16, &dataBuffer);
        if (myError != cvSuccess) {
            decodeCaenMyError(&myError);
        }

        // change uint32_t from architecture to Big Endian
        dataBuffer16 = htobe16((uint16_t)dataBuffer);

        // fill the dataPld array with the read datas with a 2 Bytes bunch
        memcpy(&lc2249w.dataPld[A*2], &dataBuffer16, sizeof(dataBuffer16));


        // check statusQ of each camac cycle AND the previous status.
        // 1 is default value; in case of Q=0 ===> Qvalue = 0 and the
        // value of all read channels is not confirmed (global statusQ = 0)
        lc2249w.statusQ &= statusQ(Handle);
      }

      // put statusQ in event dataErrMask (uint16_t)
      event.dataErrMask |= ((uint16_t)lc2249w.statusQ << (15 - lc2249w.statusQ_index));

      // add 8 Bytes of padding: "AAAAAAAA"
      dataBuffer16 = 0x4141;        // ascii "AA" characters
      dataBuffer16 = htobe16(dataBuffer16);
      for(A=0; A<4; A++) {
        memcpy(&lc2249w.dataPld[24+(A*2)], &dataBuffer16, sizeof(dataBuffer16));
      }

      if(DBGMODE) {
        printf(" + done!\n");
      }

      if(DBGMODE) {
        printf("Moving lc2249w's data ...");
      }
      // fill the event structure with the description and the data
      memcpy(event.pdescription, &lc2249w.description, sizeof(TD_instrDescr));
      event.pdescription += sizeof(TD_instrDescr);                // first free element of event.description
      event.descrSize += sizeof(TD_instrDescr);
      memcpy(event.pdata, &lc2249w.dataPld, lc2249w.dataSize * sizeof(uint8_t));
      event.pdata += (lc2249w.dataSize * sizeof(uint8_t));           // first free element of event.data
      event.dataSize += (lc2249w.dataSize * sizeof(uint8_t));

      if(DBGMODE) {
        printf(" + done!\n");
      }


      /* #######################################
       *         read multi-hit pattern unit CAEN v259
       * #######################################
       */
       if(DBGMODE) {
         printf("Start reading v259 ...");
       }

       v259n.statusQ = 1;

       // 1) read and reset pattern register (0x22) - 16 bit word
       fullAddr = (v259n.baseAddr + 0x22);
       myError = CAENVME_ReadCycle(Handle, fullAddr, &dataBuffer, v259n.addressModifier, cvD16);
       if (myError != cvSuccess) {
         decodeCaenMyError(&myError);
         v259n.statusQ = 0;
       }
       // change uint16_t from architecture to Big Endian
       dataBuffer16 = htobe16((uint16_t)dataBuffer);
       // fill the dataPld array with the read datas with a 4 Bytes bunch
       memcpy(&v259n.dataPld[0], &dataBuffer16, sizeof(dataBuffer16));



       // 2) read and reset multiplicity register (0x24) - 16 bit word
       fullAddr = (v259n.baseAddr + 0x24);
       myError = CAENVME_ReadCycle(Handle, fullAddr, &dataBuffer, v259n.addressModifier, cvD16);
       if (myError != cvSuccess) {
         decodeCaenMyError(&myError);
         v259n.statusQ = 0;
       }
       // change uint16_t from architecture to Big Endian
       dataBuffer16 = htobe16((uint16_t)dataBuffer);
       // fill the dataPld array with the read datas with a 4 Bytes bunch
       memcpy(&v259n.dataPld[2], &dataBuffer16, sizeof(dataBuffer16));


       // put statusQ in event dataErrMask (uint16_t)
       event.dataErrMask |= ((uint16_t)v259n.statusQ << (15 - v259n.statusQ_index));

       // add 12 Bytes of padding: "AAAAAAAA"
       dataBuffer16 = 0x4141;        // ascii "AA" characters
       dataBuffer16 = htobe16(dataBuffer16);
       for(A=0; A<6; A++) {
         memcpy(&v259n.dataPld[4+(A*2)], &dataBuffer16, sizeof(dataBuffer16));
       }

       if(DBGMODE) {
         printf(" + done!\n");
       }


       if(DBGMODE) {
         printf("Moving v259n's data ...");
       }
       // fill the event structure with the description and the data
       memcpy(event.pdescription, &v259n.description, sizeof(TD_instrDescr));
       event.pdescription += sizeof(TD_instrDescr);                // first free element of event.description
       event.descrSize += sizeof(TD_instrDescr);
       memcpy(event.pdata, &v259n.dataPld, v259n.dataSize * sizeof(uint8_t));
       event.pdata += (v259n.dataSize * sizeof(uint8_t));           // first free element of event.data
       event.dataSize += (v259n.dataSize * sizeof(uint8_t));


       if(DBGMODE) {
         printf(" + done!\n");
       }


       /* #######################################
        *         programmable delay CAEN c211
        * #######################################
        */

       if(DBGMODE) {
         printf("Moving c211's data ...");
       }
       // fill the event structure with the description and the data
       memcpy(event.pdescription, &c211.description, sizeof(TD_instrDescr));
       event.pdescription += sizeof(TD_instrDescr);                // first free element of event.description
       event.descrSize += sizeof(TD_instrDescr);
       memcpy(event.pdata, &c211.dataPld, c211.dataSize * sizeof(uint8_t));
       event.pdata += (c211.dataSize * sizeof(uint8_t));           // first free element of event.data
       event.dataSize += (c211.dataSize * sizeof(uint8_t));

       // as the configuration is not changing, the data are not reset to zero

       if(DBGMODE) {
         printf(" + done!\n");
       }

       printf("    + error mask: %#04x\n", event.dataErrMask);



      /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       *          FILL LIVE MONITOR
       * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       */
      monitor->readData(event, lc2228a, lc2249a, lc2249w, v259n, c257, lc2551, v560n, v560n_2);
      // sleep(1);


      /*
       * Resetting payloads of each board
       */

      bzero(&c257.dataPld[0], c257.dataSize * sizeof(uint8_t));
      bzero(&lc2551.dataPld[0], lc2551.dataSize * sizeof(uint8_t));
      bzero(&v560n.dataPld[0], v560n.dataSize * sizeof(uint8_t));
      bzero(&v560n_2.dataPld[0], v560n_2.dataSize * sizeof(uint8_t));
      bzero(&lc2228a.dataPld[0], lc2228a.dataSize * sizeof(uint8_t));
      bzero(&lc2249a.dataPld[0], lc2249a.dataSize * sizeof(uint8_t));
      bzero(&lc2249w.dataPld[0], lc2249w.dataSize * sizeof(uint8_t));
      bzero(&v259n.dataPld[0], v259n.dataSize * sizeof(uint8_t));


      /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       *          W R I T E   D A T A
       * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        #include <stdio.h>
        fwrite(address_data,size_data,numbers_data,pointer_to_file);
        - address_data is a pointer to the variable (it can be a struct) that holds data
        - size_data = sizeof(_myVariable_)
        - numbers_data is how many time do you want write the dataPad (in this case, 1)
        - pointer_to_file is FILE *pfile;
       */

      if(DBGMODE) {
        printf("Writing data to buffer ...");
      }

      eventSize = 32;         // 9 = 6 (eventSize, eventNumer) + 3 (HHH)
      eventSize += (event.descrSize + event.dataSize);

      event.eventNumber = htobe32(eventCounter);
      event.eventSize = htobe16(eventSize);
      event.dataErrMask = htobe16(event.dataErrMask);

      if(DBGMODE) {
        printf(" event.descrSize: %zu, event.dataSize: %zu\n", event.descrSize, event.dataSize);
      }

      byteWrote += fwrite(&event.eventSize, sizeof(uint8_t), 2, pmyFile);
      byteWrote += fwrite(&event.eventNumber, sizeof(uint8_t), 4, pmyFile);
      byteWrote += fwrite(&event.dataErrMask, sizeof(uint8_t), 2, pmyFile);
      byteWrote += fwrite("AAAAAAAA", sizeof(uint8_t), 8, pmyFile);
      byteWrote += fwrite(&event.description, sizeof(uint8_t), event.descrSize, pmyFile);
      byteWrote += fwrite(&event.data, sizeof(uint8_t), event.dataSize, pmyFile);
      byteWrote += fwrite("HHHHHHHHHHHHHHHH", sizeof(uint8_t), 16, pmyFile);

      if(DBGMODE) {
        printf(" + done!\n");
      }

      // cler event structure for the next event
      bzero(&event, sizeof(TD_event));
      event.pdescription = &event.description[0];
      event.pdata = &event.data[0];

      if(DBGMODE) {
        printf(" wrote %ld Bytes\n",byteWrote);
      }

      // set Inhibit on camac
      inhibit(Handle, CR, unsetINH);

      if(DBGMODE) {
        printf(" clear busy ...");
      }

      clearCorbo(Handle, &corbo);
      triggered = 0;

      if(DBGMODE) {
        printf(" + done!\n\n");
      }
    }         // end of while loop

  printf("Registered %d events\n", eventCounter);

  // Save monitoring histograms
  monitor->saveFile();
  delete monitor;
  

  myError = CAENVME_End(Handle);
  if (myError != cvSuccess) {
    decodeCaenMyError(&myError);
    return(myError);
  }

  fclose(pmyFile);      // close file
  return 0;
}




/*
 * check for the number of triggered events in the Corbo module
 */
uint8_t pollingCorbo(int32_t Handle, TD_board *corbo) {

  uint8_t triggered = 0;
  uint16_t data = 0;                      // read data
  uint32_t fullAddr = 0;
  CVErrorCodes myError = CVErrorCodes::cvSuccess;

  fullAddr = (corbo->baseAddr + 0x00);
  myError = CAENVME_ReadCycle(Handle, fullAddr, &data, cvA24_U_DATA, cvD16);
  if (myError != cvSuccess) {
    decodeCaenMyError(&myError);
  }
  if((data & 0x0200) == 0x0200) {
    triggered += 1;
  }

  data = 0;
  fullAddr = (corbo->baseAddr + 0x02);
  myError = CAENVME_ReadCycle(Handle, fullAddr, &data, cvA24_U_DATA, cvD16);
  if (myError != cvSuccess) {
    decodeCaenMyError(&myError);
  }
  if((data & 0x0200) == 0x0200) {
    triggered += 1;
  }

  data = 0;
  fullAddr = (corbo->baseAddr + 0x04);
  myError = CAENVME_ReadCycle(Handle, fullAddr, &data, cvA24_U_DATA, cvD16);
  if (myError != cvSuccess) {
    decodeCaenMyError(&myError);
  }
  if((data & 0x0200) == 0x0200) {
    triggered += 1;
  }

  data = 0;
  fullAddr = (corbo->baseAddr + 0x06);
  myError = CAENVME_ReadCycle(Handle, fullAddr, &data, cvA24_U_DATA, cvD16);
  if (myError != cvSuccess) {
    decodeCaenMyError(&myError);
  }
  if((data & 0x0200) == 0x0200) {
    triggered += 1;
  }

return triggered;
}



/*
 * clear busy signal on corbo
 */
void clearCorbo(int32_t Handle, TD_board *corbo) {

  uint16_t data = 0;                      // writing data
  uint32_t fullAddr = 0;
  CVErrorCodes myError = CVErrorCodes::cvSuccess;

  fullAddr = (corbo->baseAddr + 0x58);
  myError = CAENVME_WriteCycle(Handle, fullAddr, &data, cvA24_U_DATA, cvD16);
  if (myError != cvSuccess) {
   decodeCaenMyError(&myError);
  }

  fullAddr = (corbo->baseAddr + 0x5A);
  myError = CAENVME_WriteCycle(Handle, fullAddr, &data, cvA24_U_DATA, cvD16);
  if (myError != cvSuccess) {
   decodeCaenMyError(&myError);
  }

  fullAddr = (corbo->baseAddr + 0x5C);
  myError = CAENVME_WriteCycle(Handle, fullAddr, &data, cvA24_U_DATA, cvD16);
  if (myError != cvSuccess) {
   decodeCaenMyError(&myError);
  }

  fullAddr = (corbo->baseAddr + 0x5E);
  myError = CAENVME_WriteCycle(Handle, fullAddr, &data, cvA24_U_DATA, cvD16);
  if (myError != cvSuccess) {
   decodeCaenMyError(&myError);
  }

}


/*
 * decodeCaenMyError
 */
void decodeCaenMyError(CVErrorCodes *pmyError) {
  char myErrorString[500];
  bzero(myErrorString, 500*sizeof(char));         // inizializzo MyErrorString
  strcpy(myErrorString, CAENVME_DecodeError(*pmyError));

  if(DBGMODE) {
    printf("--> debug: function decodeCaenMyError\n");
    printf("\nIl codice di errore e': %d\n"
           "\t==> %s <==\n\n", *pmyError, myErrorString);
    printf("\nPremi Invio per continuare\n");
    getchar();
  } else {
    printf("error %d: %s\n", *pmyError, myErrorString);
  }
}


/*
 * send signal Z (initialization) to the all camac modules in crate CR
 * CBD 8210: commands CRx N28 A8 F26
*/
void camacZ(int32_t Handle, uint8_t CR) {
  if(DBGMODE) {
    printf("sending camac reset (Z) to CR%d ...", CR);
  }

  uint8_t N = 28;
  uint8_t A = 8;
  uint8_t F = 26;

  // returned value for CAENVMElib's functions
  CVErrorCodes myError;
  uint32_t dataBuffer = 0;

  myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, R16, &dataBuffer);
  if (myError != cvSuccess) {
      decodeCaenMyError(&myError);
  }

  // check status X
  if ((dataBuffer & 0x00004000) != 0x00004000) {
    printf("\nERROR: status X = 0 (command not accepted)\n");
  }

  if(DBGMODE) {
    printf("done!\n");
  }

  return;
}


/*
 * send signal C (clear) to the all camac modules in crate CR
 * CBD 8210: commands CRx N28 A9 F26
*/
void camacC(int32_t Handle, uint8_t CR) {
  if(DBGMODE) {
    printf("sending camac clear (C) to CR%d ...", CR);
  }

  uint8_t N = 28;
  uint8_t A = 9;
  uint8_t F = 26;

  // returned value for CAENVMElib's functions
  CVErrorCodes myError;
  uint32_t dataBuffer = 0;

  myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, R16, &dataBuffer);
  if (myError != cvSuccess) {
      decodeCaenMyError(&myError);
  }

  // check status X
  if ((dataBuffer & 0x00004000) != 0x00004000) {
    printf("\nERROR: status X = 0 (command not accepted)\n");
  }

  if(DBGMODE) {
    printf("done!\n");
  }

  return;
}


/*
 * statusQ:
 * controllare lo status Q (stato dell'ultima operazione di lettura camac)
 * sul CES 8210 controllando il bit 15 del registro CSR accessibile con i
 * seguenti parametri: CR0 N29 A0 F0
 */
uint8_t statusQ(int32_t Handle) {
  uint8_t CR = 0;
  uint8_t N = 29;
  uint8_t A = 0;
  uint8_t F = 0;

  // returned value for CAENVMElib's functions
  CVErrorCodes myError = CVErrorCodes::cvSuccess;
  uint32_t dataBuffer = 0;
  uint8_t Q = 0;

  myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, R16, &dataBuffer);
  if (myError != cvSuccess) {
      decodeCaenMyError(&myError);
  }
  if ( (dataBuffer & 0x00008000) == 0x00008000 ) {
    Q = 1;
  }
  else {
    Q = 0;
  }

  if(DBGMODE) {
    printf("status of last camac cycle: Q = %d\n", Q);
  }

  return Q;
}


/*
 * statusX:
 * controllare lo status Q (stato dell'ultima operazione di lettura camac)
 * sul CES 8210 controllando il bit 14 del registro CSR accessibile con i
 * seguenti parametri: CR0 N29 A0 F0
 */
uint8_t statusX(int32_t Handle) {
  uint8_t CR = 0;
  uint8_t N = 29;
  uint8_t A = 0;
  uint8_t F = 0;

  // returned value for CAENVMElib's functions
  CVErrorCodes myError = CVErrorCodes::cvSuccess;
  uint32_t dataBuffer = 0;
  uint8_t X = 0;

  myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, R16, &dataBuffer);
  if (myError != cvSuccess) {
      decodeCaenMyError(&myError);
  }
  if ( (dataBuffer & 0x00004000) == 0x00004000 ) {
    X = 1;
  }
  else {
    X = 0;
  }

  if(DBGMODE) {
    printf("status of last camac cycle: X = %d\n", X);
  }

  return X;
}


/*
 * inhibit:
*/
TD_inhibit inhibit(int32_t Handle, uint8_t CR, TD_inhibit inhibit) {
  uint8_t N = 0;
  uint8_t A = 0;
  uint8_t F = 0;

  // returned value for CAENVMElib's functions
  CVErrorCodes myError = CVErrorCodes::cvSuccess;
  uint32_t dataBuffer = 0;

  if(inhibit == setINH) {
    /*
    set camac dataway inhibit (I) from the camac busy
    CBD 8210: commands CR1 N30 A9 F26
    */
    N = 30;
    A = 9;
    F = 26;

    myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, R16, &dataBuffer);
    if (myError != cvSuccess) {
        decodeCaenMyError(&myError);
    }

    // check status X
    if ((dataBuffer & 0x00004000) != 0x00004000) {
      // command not accepted
      printf("\nERROR: status X = 0 (command not accepted)\n");
      return setINH;
    }
    else {
      if(DBGMODE) {
        printf("inhibit on CR%d is unset\n", CR);
      }

      return unsetINH;
    }

  }
  else {
    /*
    remove camac dataway inhibit (I) from the camac busy
    CBD 8210: commands CR1 N30 A9 F24
    */
    N = 30;
    A = 9;
    F = 24;

    myError = (CVErrorCodes) VME_CAMAC_command(Handle, CR, N, A, F, R16, &dataBuffer);
    if (myError != cvSuccess) {
        decodeCaenMyError(&myError);
    }

    // check status X
    if ((dataBuffer & 0x00004000) != 0x00004000) {
      // command not accepted
      printf("\nERROR: status X = 0 (command not accepted)\n");
      return unsetINH;
    }
    else {
      if(DBGMODE) {
        printf("inhibit on CR%d is set\n", CR);
      }

      return setINH;
    }
  }
}

