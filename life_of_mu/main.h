/*
 * for debugging purposes: if compiled with -D _DBGMODE
 * then enables the prints
 */
#ifdef _DBGMODE
#define DBGMODE 1
#else
#define DBGMODE 0
#endif

#ifndef MAIN_H
#define MAIN_H


#define INSTRDESCRSIZE 64     // size of instrument description
#define MAX_PAYLOAD 256       // max size of data from a single board
#define MAX_INSTRUMENTS 10    // max number of instrument

#include <stdint.h>
#include <CAENVMEtypes.h>

//defines the size of the description field of each board
typedef char TD_instrDescr[INSTRDESCRSIZE];

// definition of a generic board
typedef enum { camac,
               vme
             } boardType_t;

typedef struct {
  boardType_t type;
  TD_instrDescr description;
  uint32_t baseAddr;
  size_t dataSize;                // dimension in Bytes of the real payload
  uint8_t dataPld[MAX_PAYLOAD];
  uint8_t statusQ_index;          // index of the module in the dataErrMask of each event
  uint8_t statusQ;
  CVAddressModifier addressModifier;
} TD_board;

// struct that hods the event
typedef struct {
  uint16_t eventSize;                           // 2 Bytes
  uint32_t eventNumber;                         // 4 Bytes
  uint16_t dataErrMask;                         // 2 Bytes
  uint8_t description[MAX_INSTRUMENTS * INSTRDESCRSIZE];
  uint8_t* pdescription;                        // points to first free element of description
  size_t descrSize;                             // size of the description in Bytes
  uint8_t data[MAX_INSTRUMENTS * MAX_PAYLOAD];
  uint8_t* pdata;                               // points to first free element of data
  size_t dataSize;                              // size of the data in Bytes
} TD_event;


// new typedef for inhibit signal
typedef enum { setINH,
               unsetINH
              } TD_inhibit;

/*
 * this function decodes the error stored in myError.
 * The tye CVErrorCodes is defined in CAENVMElib.h
 */
void decodeCaenMyError(CVErrorCodes *myError);


/*
 * read an asynchronous input from keyboard
 */
int kbHit();

/*
 * polls the corbo module until there is a trigger.
 * As argument needs the handler of V1718 and returns a positive number if
 * there is a trigger.
 */
uint8_t pollingCorbo(int32_t Handle, TD_board *corbo);

/*
 * clears the busy signal on corbo module
 */
void clearCorbo(int32_t Handle, TD_board *corbo);

/*
 * check the status Q. If true, data is validated.
 */
uint8_t statusX(int32_t Handle);

/*
 * check the status Q. If true, data is validated.
 */
uint8_t statusQ(int32_t Handle);

/*
 * set / unset inhibit signal on camac
 */
TD_inhibit inhibit(int32_t Handle, uint8_t CR, TD_inhibit inhibit);

/*
 * send Z (reset) command to crate CRx
 */
void camacZ(int32_t Handle, uint8_t CR);

/*
 * send C (clear) command to crate CRx
 */
void camacC(int32_t Handle, uint8_t CR);

#endif // MAIN_H
