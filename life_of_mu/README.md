# Program for data acquisition in the muon-lifetime experiment

The program regularly queries the CORBO until an event was triggered, and then reads data from each channel of each configured VME and CAMAC board.
The data is written in binary *big-endian* format to a buffer file and sent to the monitoring module to fill all relevant histograms.
See more on endianness below.

Every time the program is started, the buffer file is created with current date/time in the name to avoid overwriting existing files.
When program is stopped, canvas with monitoring histograms is saved to a `pdf` file with the same name as the buffer file.

### Compiling the DAQ program 

* execute in terminal: `make all`
* an executable will be created: `main.exe`

### Running the data acquisition

1. ensure that VME bridge CAEN V1718 is powered on and connected to the laptop via a USB cable
2. ensure that USB magic number for the bridge is `0` by looking for it in the list of devices: `ls -1 /dev/`
   * should see a file `0_V1718` in the list
   * otherwise restart the laptop with VME bridge connected to the laptop
3. start data-acquisition: `./main.exe`
   * printouts will appear in the terminal after each triggered event
   * a ROOT canvas will open in a new window, showing live histograms filled with recorded data
   * NOTE: on the laboratory laptop it is required to execute `source setenv.sh` at the beginning of session to properly set library paths
4. stop data acquisition by sending SIGINT signal (pressing `CTRL+C`) in the terminal window
   * buffer file with binary data from detectors will be closed
   * canvas with histograms will be saved as a PDF file


### Proper treatment of endianness

For better readability by eye of data written to the buffer, each value is written to the buffer in the *[big-endian](https://en.wikipedia.org/wiki/Endianness)* format.
Most likely the architecture on which the analysis will be performed will be using the *little-endian* format for data management.
Therefore, when reading data from the buffer file, it is required to convert each value to the host format of your computer.

**Endianness conversion in C or C++**

It is possible to convert values between different formats using function defined in [`endian.h`](http://man7.org/linux/man-pages/man3/endian.3.html)

```c++
#include <stdio.h>
#include <stdint.h>
#include <endian.h>

int main()
{
    uint16_t be_16bit = 0x1234;
    uint32_t be_32bit = 0x12345678;
    uint64_t be_64bit = 0x123456789ABCDEF;
    
    // Converting from big-endian to host endianness
    uint16_t h_16bit = be16toh(be_16bit);
    uint32_t h_32bit = be32toh(be_32bit);
    uint64_t h_64bit = be64toh(be_64bit);
    
    printf("0x%X  ->  0x%X\n", be_16bit, h_16bit);        // 0x1234  ->  0x3412
    printf("0x%X  ->  0x%X\n", be_32bit, h_32bit);        // 0x12345678  ->  0x78563412
    printf("0x%lX  ->  0x%lX\n", be_64bit, h_64bit);      // 0x123456789ABCDEF  ->  0xEFCDAB8967452301
    
    return 0;
}
```
> NOTE: In the 64bit variable the 2 least significant bits are `01`, instead of `12`, because the `be_64bit` variable is initialised with only 15 digits, 
leaving the most significant bit in big-endian format equal to `0`.  
> NOTE: You can test the interactive version of this code snippet online at [onlinegdb.com](https://onlinegdb.com/rk4SLB1wG).

**Reading binary data with proper endianness in Python**

Python allows to read any number of bytes from a binary file into a binary string.
The [`struct`](https://docs.python.org/2/library/struct.html) module can be used to extract a sequence of blocks from the binary 
string as a tuple of values according to endianness and data type defined in a format string.

Consider the following binary file `sample.dat`, with 16 bytes/event written in big-endian format:

```
03c0 0000 0001 ff80 4141 4141 4141 4141
03c0 0000 0002 ff00 4141 4141 4141 4141
03c0 0000 0003 ff00 4141 4141 4141 4141

```

The following code snippet reads each block of 16 bytes in big-endian format (defined by `>`) and splits it into 4 variables of the following size:
`H:2 bytes` `I:4 bytes` `H:2 bytes` `Q:8 bytes`, according to the [format characters](https://docs.python.org/2/library/struct.html#format-characters).

```python
import struct

with open('sample.dat', 'rb') as file_in:
	while True:
		# Read 16 bytes
		b = file_in.read(16)
		# Check whether not reached end of file
		if b == '':
			break
		# Extract blocks of bytes into separate variables, using big-endian format
		# H:2 bytes | I:4 bytes | H:2 bytes | Q:8 bytes
		event_size, event_number, q_status, padding = struct.unpack('>HIHQ', b)
		print("{0:d}  {1:d}  {2:b} {3:#x}".format(event_size, event_number, q_status, padding))
```

Will print:

`960  1  1111111110000000 0x4141414141414141`  
`960  2  1111111100000000 0x4141414141414141`  
`960  3  1111111100000000 0x4141414141414141`
