/*
    Library for live monitoring of data taking using ROOT histograms
*/

#ifndef MONITOR_H
#define MONITOR_H

#define MONITOR_LIVE_UPDATES false         // in seconds
#define MONITOR_LIVE_UPDATE_PERIOD 0.2     // in seconds
#define MONITOR_SAVE_PERIOD 60*60           // in seconds

#include <iostream>
#include <string>
#include <map>
#include <time.h>
#include <TH1.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TApplication.h>
#include <TSystem.h>

#include "main.h"

// Styling variables
const Color_t COLORS[6] = {kAzure+2, kRed, kSpring-6, 12, kPink+2, kOrange-5};          // https://root.cern.ch/doc/master/classTColor.html#C02
const Color_t FILL_COLORS[6] = {kAzure-9, kRed-9, kSpring-4, 18, kMagenta-10, kOrange-9};   // https://root.cern.ch/doc/master/classTColor.html#C02
const int LINE_STYLES[4] = {1, 9, 7, 4};                                                // https://root.cern.ch/doc/master/classTAttLine.html#L3
const int MARKER_STYLES[5][2] = {{20, 24}, {21, 25}, {22, 26}, {23, 32}, {34, 28}};  // https://root.cern.ch/doc/master/classTAttMarker.html#M2

// Struct that holds the relevant event info
typedef struct {
  uint16_t dataErrMask;                         // 16 bit
  uint16_t data_tdc[8];                         // 8 channels x 16 bit
  uint16_t data_adc_lc2249a[12];                // 12 channels x 16 bit
  uint16_t data_adc_lc2249w[12];                // 12 channels x 16 bit
  uint32_t data_scaler_c257[16];                // 16 channels x 32 bit
  uint32_t data_scaler_v560n[16];               // 16 channels x 32 bit
  uint32_t data_scaler_v560n2[16];              // 16 channels x 32 bit
  uint16_t data_pattern_unit[2];                // 16 bit + 16 bit

} TD_monitor_event;

// Struct that holds configurations of additional axes
typedef struct {
    int pad;
    bool horizontal;
    Float_t range[2];
    char title[150];
} TD_axis_config;


// Monitor class
class Monitor
{
public:
    Monitor(const char *fileName);
    int n_events;
    void saveFile(const char *append="");
    void readData(const TD_event &event, const TD_board &lc2228a, const TD_board &lc2249a, const TD_board &lc2249w, 
                  const TD_board &v259n, const TD_board &c257, const TD_board &lc2551, 
                  const TD_board &v560n, const TD_board &v560n_2);
private:
    bool busy;
    TD_monitor_event event;
    time_t last_updated;
    time_t last_saved;
    std::string fileNameBase;
    std::map<std::string, TH1*> histos;
    TApplication* app;
    TSystem* sys;
    TCanvas *canvas;
    std::map<int, TGaxis*> add_axes;
    void fillEvent();
    void bookHistos();
    void initCanvas();
    void updateCanvas();
    void setStyle();
    void drawHistos(int pad, const std::vector<int> &channels, const char *nameTemplate, int stats=1);
};

#endif // MONITOR_H
