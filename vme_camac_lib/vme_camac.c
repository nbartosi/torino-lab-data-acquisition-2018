#ifndef VME_CAMAC_C
#define VME_CAMAC_C

#include <inttypes.h>
#include <string.h>

#include "vme_camac.h"


// Creates a handle to be used in further communication with CAMAC modules
CAENComm_ErrorCode VME_CAMAC_init(uint32_t *handle) {
    CVErrorCodes status = CAENVME_Init(VME_CAMAC_BOARD, 
                                      VME_CAMAC_LINK, 
                                      VME_CAMAC_DEVICE, 
                                      handle);
    if (status != cvSuccess) {
        printf("ERROR: VME CAMAC initialisation failed\n");
        printf("%s\n", CAENVME_DecodeError(status));
    }

    return VME_to_Comm_error(status);
}


// Sends a read or write command to CAMAC address defined using N A F values
CAENComm_ErrorCode VME_CAMAC_command(uint32_t handle, 
                                     uint32_t N,
                                     uint32_t A,
                                     uint32_t F,
                                     const char *command,
                                     uint32_t *data) {
    // Constructing the offset address from N A F values
    uint32_t address_offset = 0;
    address_offset |= N << 11;   // CAMAC station number: bits 15..11
    address_offset |= A << 7;   // CAMAC sub-address: bits 10..7
    address_offset |= F << 2;   // function code: bits 6..2

    CVErrorCodes status = cvInvalidParam;

    if (strcmp(command, "r16") == 0) {
        // Reading a 16bit word in one cycle using 16bit addressing: bit 1 = 1
        address_offset |= 1 << 1;
        status = CAENVME_ReadCycle(handle, VME_CAMAC_BASE_ADDRESS | address_offset, data, cvA24_U_DATA, cvD16);
    } else
    if (strcmp(command, "r24") == 0) {
        // Reading a 16bit word from higher part of 32bit word using 24bit addressing: bit 1 = 0
        uint32_t data_fraction = 0;
        status = CAENVME_ReadCycle(handle, VME_CAMAC_BASE_ADDRESS | address_offset, &data_fraction, cvA24_U_DATA, cvD16);
        *data = data_fraction << 16;
        if (status == cvSuccess) {
            // Reading a 16bit word from lower part of 32bit word using 16bit addressing: bit 1 = 1
            address_offset |= 1 << 1;
            data_fraction = 0;
            status = CAENVME_ReadCycle(handle, VME_CAMAC_BASE_ADDRESS | address_offset, &data_fraction, cvA24_U_DATA, cvD16);
            *data += data_fraction;
        }
    } else 
    if (strcmp(command, "w16") == 0) {
        // Writing a 16bit word in one cycle using 16bit addressing: bit 1 = 1
        address_offset |= 1 << 1;
        status = CAENVME_WriteCycle(handle, VME_CAMAC_BASE_ADDRESS | address_offset, data, cvA24_U_DATA, cvD16);
    } else
    if (strcmp(command, "w24") == 0) {
        // Writing a 16bit word from higher part of 32bit word using 24bit addressing: bit 1 = 0
        uint32_t data_fraction = *data >> 16;
        status = CAENVME_WriteCycle(handle, VME_CAMAC_BASE_ADDRESS | address_offset, &data_fraction, cvA24_U_DATA, cvD16);
        if (status == cvSuccess) {
            // Reading a 16bit word from lower part of 32bit word using 16bit addressing: bit 1 = 1
            address_offset |= 1 << 1;
            data_fraction = *data & 0xFFFF;   // masking only the last 16 bits of the initial 32bit value
            status = CAENVME_WriteCycle(handle, VME_CAMAC_BASE_ADDRESS | address_offset, &data_fraction, cvA24_U_DATA, cvD16);
        }
    } else {
        printf("ERROR: wrong command.\nShould be one of: r16|r24|w16|w24\n");
    }

    // Decoding the status
    if (status != cvSuccess) {
        printf("ERROR: Command [%s] to address [0x%.8X] with data [0x%.6X] failed\n", command, VME_CAMAC_BASE_ADDRESS | address_offset, *data);
        printf("%s\n", CAENVME_DecodeError(status));
    }
    
    return VME_to_Comm_error(status);
}


// Maps error codes from CAEN VME library to error codes in CAEN Common library
CAENComm_ErrorCode VME_to_Comm_error(CVErrorCodes error_vme) {
    CAENComm_ErrorCode error_comm = 0;

    switch (error_vme) {
        case cvTimeoutError:
            error_comm = CAENComm_CommTimeout;
            break;
        default:
            // integer enum values are identical for all cases except -5, therefore simple cast is enough
            error_comm = (CAENComm_ErrorCode) error_vme;
            break;
    }

    return error_comm;
}


#endif // VME_CAMAC_C
