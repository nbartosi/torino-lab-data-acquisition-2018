/*
	This library allows to read or write 16bit or 24bit data to a CAMAC address via a VME bridge
*/

#ifndef VME_CAMAC_H
#define VME_CAMAC_H

#include <stdint.h>

#include "CAENVMElib.h"
#include "CAENComm.h"

// Base address used for addressing of CAMAC modules trough VME CAMAC branch driver [CES CBD 8210]
// corresponds to the specific positions and configurations of modules in the setup
#define VME_CAMAC_BASE_ADDRESS 0x810000

// Defining constants for initialisation of connection to CAMAC through VME
#define VME_CAMAC_LINK 0   // irrelevant for V1718
#define VME_CAMAC_DEVICE 0   // slot number where the V1718 is installed
#define VME_CAMAC_BOARD cvV1718   // type of the VME interface board [defined in CAENVMEtypes.h]


// Creates a handle to be used in further communication with CAMAC modules
CAENComm_ErrorCode VME_CAMAC_init(uint32_t *handle);

// Sends a read or write command to CAMAC address defined using N A F values
CAENComm_ErrorCode VME_CAMAC_command(uint32_t handle, 
                                     uint32_t N,
                                     uint32_t A,
                                     uint32_t F,
                                     const char *command,
                                     uint32_t *data);

// Maps error codes from CAEN VME library to error codes in CAEN Common library
CAENComm_ErrorCode VME_to_Comm_error(CVErrorCodes error_vme);


#endif // VME_CAMAC_H
