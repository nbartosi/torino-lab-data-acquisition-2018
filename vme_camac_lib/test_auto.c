/*
	Simple program for testing of the VME_CAMAC library by addressing a CAEN C222 CAMAC Dataway Tester.

	Performs several write/read cycles with different values, ensuring that the written and read values match.
*/

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "vme_camac.h"

int main(int argc, char** argv) {
	CAENComm_ErrorCode status = CAENComm_InvalidParam;

	if (argc < 2) {
		printf("Usage:\n  ./test <CAMAC dataway station number>\n");
		return 1;
	}

	// Initialising the handle
	uint32_t handle = 0;
	status = VME_CAMAC_init(&handle);
	if (status != CAENComm_Success) {
		return (int)status;
	}

	// Defining N A values
	uint32_t N = atoi(argv[1]);
	uint32_t A = 0;

	// Defining the variable holding data to be read/written
	uint32_t data_write = 0;
	uint32_t data_read = 0;

	uint n_total = 0;
	uint n_ok = 0;

	// Testing 16bit
	uint32_t datas_16[3] = {0xF5F3, 0x0};
	for (int i = 0; i<2; ++i) {
		data_write = datas_16[i];
		printf("Writing and reading 16bit data: 0x%.4X\n", data_write);
		status = VME_CAMAC_command(handle, N, A, 16, "w16", &data_write);
		if (status == CAENComm_Success) {
			status = VME_CAMAC_command(handle, N, A, 0, "r16", &data_read);
		}
		n_total += 1;
		if (status == CAENComm_Success && data_read == data_write) {
			printf("  OK\n");
			n_ok += 1;
		} else {
			printf("  ERROR\n  written 0x%.4X != 0x%.4x read\n", data_write, data_read);
		}
		printf("    Press ANY KEY to continue...");
		getchar();
	}

	// Testing 24bit
	uint32_t datas_24[4] = {0xFAF5F3, 0xF5F3, 0x0};
	for (int i = 0; i<3; ++i) {
		data_write = datas_24[i];
		printf("Writing and reading 24bit data: 0x%.6X\n", data_write);
		status = VME_CAMAC_command(handle, N, A, 16, "w24", &data_write);
		if (status == CAENComm_Success) {
			status = VME_CAMAC_command(handle, N, A, 0, "r24", &data_read);
		}
		n_total += 1;
		if (status == CAENComm_Success && data_read == data_write) {
			printf("  OK\n");
			n_ok += 1;
		} else {
			printf("  ERROR\n  written 0x%.6X != 0x%.6x read\n", data_write, data_read);
		}
		if (i < 2) {
			printf("    Press ANY KEY to continue...");
			getchar();
		}
	}

	printf("\nAll tests done:  %d/%d OK\n", n_ok, n_total);

	return 0;
}