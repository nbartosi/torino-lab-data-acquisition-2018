# Library for CAMAC addressing via a VME bridge

This library provides a simple way to send read/write commands to any CAMAC module connected using the setup described below.
Certain technicalities like writing a 24bit value as a sequence of 16bit values and construction of address in the CAMAC space are taken care of by the library.

## Hardware configuration

In order to communicate with CAMAC modules, the following chain of devices is used:

1. **[CAEN V1718](http://www.caen.it/jsp/Template2/CaenProd.jsp?parent=11&idmod=417)** - VME-USB2.0 Bridge
    * inserted in `VME` bus, station `1`
        * required in order to operate as a VME crate controller
    * connects to a PC via the USB interface
    * allows to address other VME modules from a PC using the [CAENComm](http://www.caen.it/csite/CaenProd.jsp?parent=38&idmod=684) library
2. **CES CBD 8210** - CAMAC Branch Driver
    * inserted in `VME` bus
    * connects to the **CES CCA2 2110** CAMAC Crate Controller through a cable
    * allows to address CAMAC bus from VME bus
    * has a switch for setting a `branch number`: `0` in our setup
        * affects the `VME_CAMAC_BASE_ADDRESS` in `vme_camac.h`
3. **CES CCA2 2110** - CAMAC Crate Controller
    * inserted in `CAMAC` bus, station `24`
        * must be inserted in the last station to operate as a CAMAC crate controller
    * controls other modules in the same CAMAC crate
    * has a switch for setting a `crate address`: `1` in our setup
        * affects the `VME_CAMAC_BASE_ADDRESS` in `vme_camac.h`
4. **CAEN C222** - CAMAC Dataway Tester
    * inserted in `CAMAC` bus
    * monitors the flow of data through the CAMAC bus



## For deeper understanding

### CAMAC Branch Driver addressing scheme and base address

Format of the address (see [CES CBD 8210](FIXME), p. 1):
   
* `10` `branch number: 3 bit` `crate number: 3 bit` `N (station): 5 bit` `A (address): 4 bit` `F (function code): 5 bit` `word size: 1 bit` `0`

The `branch number` and `crate number` stay fixed during experiment, and define the base address for CAMAC bus in VME address space.
Since these parameters will not change, the base address is hardcoded in hexadecimal form in `vme_camac.h`.

The base address is constructed by filling in the fixed parameters in binary form, leaving all other parameters as `0`, and converting it to hexadecimal form:

* `branch number`: `0` -> `000`
* `crate number`: `1` -> `001`

The full base address is:

* binary: `10` `000` `001` `00000` `0000` `00000` `00`
* hexadecimal: `810000`
