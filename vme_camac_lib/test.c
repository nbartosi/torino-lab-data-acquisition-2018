/*
	Simple program for testing of the VME_CAMAC library by addressing a CAEN C222 CAMAC Dataway Tester.

	Performs several write/read cycles with different values, ensuring that the written and read values match.
*/

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "vme_camac.h"

int main(int argc, char** argv) {
	CAENComm_ErrorCode status = CAENComm_InvalidParam;

	if (argc < 5) {
		printf("Usage:\n  ./test <N> <A> <F> <r16|w16|r24|w24> <data>\n");
		return 1;
	}

	// Initialising the handle
	uint32_t handle = 0;
	status = VME_CAMAC_init(&handle);
	if (status != CAENComm_Success) {
		return (int)status;
	}

	// Reading N A F values
	uint32_t N = (uint32_t)strtoll(argv[1], NULL, 0);
	uint32_t A = (uint32_t)strtoll(argv[2], NULL, 0);
	uint32_t F = (uint32_t)strtoll(argv[3], NULL, 0);
	
	char *func = argv[4];
	
	uint32_t data = 0;
	if (argc > 5) {
		data = (uint32_t)strtoll(argv[5], NULL, 0);
	}

	status = VME_CAMAC_command(handle, N, A, F, func, &data);

	if (status == CAENComm_Success) {
		printf("Data: 0x%.6X\n", data);
	} else {
		char msg[500];
		CAENComm_DecodeError(status, msg);
		printf("ERROR: %s\n", msg);
		return 1;
	}

	return 0;
}
