/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#include <CAENVMElib.h>

#include "debugging_tool.h"

/*
 * for debugging purposes: if compiled with -D _DBGMODE
 * then enables the prints
 */
#ifdef _DBGMODE
#define DBGMODE 1
#else
#define DBGMODE 0
#endif

/*
 *
 */
void decodeCaenMyError(CVErrorCodes *pmyError) {
    if(DBGMODE) {
        printf("--> debug: function decodeCaenMyError\n");
    }
    char myErrorString[500];
    bzero(myErrorString, 500*sizeof(char));         // inizializzo MyErrorString
    strcpy(myErrorString, CAENVME_DecodeError(*pmyError));
    printf("\nIl codice di errore e': %d\n"
           "\t==> %s <==\n\n", *pmyError, myErrorString);
    printf("\nPremi Invio per continuare\n");
    getchar();
}

int main(int argc, char** argv) {
    
    // returned value for CAENVMElib's functions
    CVErrorCodes myError = 0;
    
    // stuff needed for initializing the board
    CVBoardTypes BdType = cvV1718;
    short Link = 0;
    short BdNum = 0;
    int32_t Handle;
    
    // indirizzi
    uint32_t baseAddr = 0;
    uint32_t offsetAddr = 0;
    uint32_t fullAddr = 0;
    
    // "tools" variables
    int32_t userChoice = 0;
    uint32_t data = 0;
    uint32_t IOBuff = 0;    // 32 bit bi-directional buffer for CAMAC
    
    myError = CAENVME_Init(BdType, Link, BdNum, &Handle);
    
    if (myError != cvSuccess) {
        decodeCaenMyError(&myError);
        return(myError);
    }
    
    while(1) {
        
        // reset variables
        data = 0;
        userChoice = 0;
        offsetAddr = 0;
        fullAddr = 0;
        
        printf("\n\n------------------------------\n"
                "current BaseAddress: [0x%.8X]\n"
                "input 0 --> keep\n"
                "input 1 --> change\n"
                "input -1 --> quit\n", baseAddr);
        printf("? ");
        scanf("%d",&userChoice);
        
        if (userChoice < 0) {
            // quit program
            break;
        } else if (userChoice == 1) {
            // insert new BaseAddress
            printf("insert new BaseAddress [max value 0xFFFFFFFF]\n"
                    "0x");
            scanf("%x", &baseAddr);
            printf("current BaseAddress: [0x%.8X]\n\n", baseAddr);
        }



        printf("inserire N: ");
        scanf("%u", &data);
        offsetAddr = ((data & 0x1F)  << 11);
        printf("offset = 0x%.8X\n", offsetAddr);
        data = 0;

        printf("inserire A: ");
        scanf("%u", &data);
        offsetAddr += ((data & 0xF)  << 7);
        printf("offset = 0x%.8X\n", offsetAddr);
        data = 0;

        printf("inserire F: ");
        scanf("%u", &data);
        offsetAddr += ((data & 0x1F)  << 2);
        printf("offset = 0x%.8X\n", offsetAddr);
        data = 0;

        /*
        printf("inserire S: ");
        scanf("%u", &data);
        offsetAddr += ((data & 0x1)  << 1);
        printf("offset = 0x%.8X\n", offsetAddr);
        */

        /*
         * force bit #1 (16/24 bit) to 1 (16 bit).
         * it will be forced to 0 (24 bit) automatically
         */
        offsetAddr += 0x2;
        
        fullAddr = baseAddr + offsetAddr;
        printf("base+offset address: [0x%.8X]\n", fullAddr);
        
        printf("\n\nenter command:\n"
                "1 --> read_16\n"
                "2 --> read_24\n"
                "3 --> write_16\n"
                "4 --> write_24\n"
                "? \n");
        scanf("%d", &userChoice);
        
        if (userChoice == 1) {
            // read_16
            myError = CAENVME_ReadCycle(Handle, fullAddr,  &IOBuff, cvA24_U_DATA, cvD16);
            if (myError != cvSuccess) {
                decodeCaenMyError(&myError);
            } else {
                
                data = 0;           // initialize data
                IOBuff = (IOBuff & 0x0000FFFF);     // keep bits (16 --> 0)
                data = IOBuff;      // copy data from IOBuff to data
                printf("read D16: 0x%.8X\n", data);
            }
        } else if (userChoice == 2) {
            /* 
             * read_24 is composed by two cycles:
             * --> I cycle: force reading 16 HIGH bits (31 --> 24) by setting
             * the addressing bit1 to 0 (24 bit)
             * --> II cycle: read 16 LOW bits (23 --> 0) by setting the
             * addressing bit1 to 1 (16 bit)
             */
            
            /*
             * I cycle: high part (bits 31 --> 16)
             */            
            myError = CAENVME_ReadCycle(Handle, (fullAddr & 0xFFFFFFFC),  &IOBuff, cvA24_U_DATA, cvD16);
            if (myError != cvSuccess) {
                decodeCaenMyError(&myError);
            } else {
                
                data = 0;                       // initialize data
                IOBuff = (IOBuff & 0x0000FFFF);     // keep bits (15 --> 0)
                IOBuff = (IOBuff << 16);        // shift left of 16 bits
                data = IOBuff;
            /*
             * II cycle: low part (bits 15 --> 0)
             */            
                myError = CAENVME_ReadCycle(Handle, (fullAddr & 0xFFFFFFFE),  &IOBuff, cvA24_U_DATA, cvD16);
                if (myError != cvSuccess) {
                decodeCaenMyError(&myError);
                } else {

                    IOBuff = (IOBuff & 0x0000FFFF);     // keep bits (15 --> 0)
                    data = (data | IOBuff);             // bit sum of data and IOBuff

                    printf("read D32: 0x%.8X\n", data);
                }
            }
        } else if (userChoice == 3) {
            // write_16
            printf("enter data (max 16bit):\n0x");
            scanf("%x", &data);
            data = (data & 0x0000FFFF);
            myError = CAENVME_WriteCycle(Handle, fullAddr, &data, cvA24_U_DATA, cvD16);
            if (myError != cvSuccess) {
                decodeCaenMyError(&myError);
            }
        } else if (userChoice == 4) {
            // write_24
            printf("enter data (max 24bit):\n0x");
            scanf("%x", &data);
            data = (data & 0x00FFFFFF);
            /* 
             * write_24 is composed by two cycles:
             * --> I cycle: force writing 16 HIGH bits (31 --> 24) by setting
             * the addressing bit1 to 0 (24 bit)
             * --> II cycle: write 16 LOW bits (23 --> 0) by setting the
             * addressing bit1 to 1 (16 bit)
             */
            
            /*
             * I cycle: high part (bits 31 --> 16)
             */            
            IOBuff = 0;         // initialize IOBuff
            IOBuff = (data >> 16);  // copy into data bits from 31 to 16 in lower part (bits 15 --> 0) of IOBuff
            myError = CAENVME_WriteCycle(Handle, (fullAddr & 0xFFFFFFFC), &IOBuff, cvA24_U_DATA, cvD16);
            if (myError != cvSuccess) {
                decodeCaenMyError(&myError);
            } else {
                IOBuff = (data & 0x0000FFFF);     // copy into lower part (bits 15 --> 0) of IOBuff low (bits 15 --> 0) of data
                myError = CAENVME_WriteCycle(Handle, (fullAddr & 0xFFFFFFFE), &IOBuff, cvA24_U_DATA, cvD16);
                
                if (myError != cvSuccess) {
                decodeCaenMyError(&myError);
                }
            }
        }
        
        
    }
    
    
    myError = CAENVME_End(Handle);
    if (myError != cvSuccess) {
        decodeCaenMyError(&myError);
        return(myError);
    }
    
    return(0);
    
}
