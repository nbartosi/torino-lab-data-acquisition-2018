/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   debugging_tool.h
 * Author: giorgione
 *
 * Created on November 17, 2017, 3:24 PM
 */

#ifndef DEBUGGING_TOOL_H
#define DEBUGGING_TOOL_H

#ifdef __cplusplus
extern "C" {
#endif




#ifdef __cplusplus
}
#endif

#endif /* DEBUGGING_TOOL_H */


/*
 * this function decodes the error stored in myError.
 * The tye CVErrorCodes is defined in CAENVMElib.h
 */
void decodeCaenMyError(CVErrorCodes *myError);
