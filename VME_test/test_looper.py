#!/usr/bin/env python

import sys
import time
import datetime
import subprocess

def now():
	return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

def main():

	sys.stderr.write('[{0:s}] Starting to loop over potential base addresses of a CAMAC module:\n'.format(now()))

	# Range of all possible values
	branches = range(0, 7+1)
	crates = range(0, 7+1)
	stations = range(0, 24+1)

	# # Fixed values
	# branches = [0]
	# crates = [1]
	# stations = [18]

	a_address = 0
	function_code = 16   	# write word
	word_size = 1			# 0 - 24 bit; 1 - 16 bit
	word = '0x5555'			# 0101010101010101 [first 16 LEDs of the CAMAC dataway tester]


	count = {
		'checked': 0,
		'success': 0,
		'failure': 0
	}
	statuses = set()

	n_total = len(branches) * len(crates) * len(stations)

	for branch in branches:
		for crate in crates:
			for station in stations:
				# if count['checked'] > 1:
				# 	break
				address = '10{0:03b}{1:03b}{2:05b}{3:04b}{4:05b}{5:01b}0'.format(branch, crate, station, a_address, function_code, word_size)
				# address = '10{0:03b}{1:03b}{2:05b}{3:04b}{4:05b}{5:01b}'.format(branch, crate, station, a_address, function_code, word_size)
				address_hex = hex(int(address, 2))
				sys.stderr.write('##### Checking address {5:d}/{6:d} for [branch: {0:d}, crate: {1:d}, station: {2:d}]:  {3:s} > {4:s}\n'.format(branch, crate, station, address, address_hex, count['checked'], n_total))
				result = subprocess.call(['./vme_ping.exe', '0', address_hex, word])
				statuses.add(result)
				count['checked'] += 1
				if result == 0:
					# sys.stderr.write('  SUCCESS\n')
					count['success'] += 1
				else:
					# sys.stderr.write('  FAILURE\n')
					count['failure'] += 1
				# Wait for 50ms
				# time.sleep(0.1)
	print('\n[{0:s}] Finished checking {1:d} addresses:'.format(now(), count['checked']))
	print('      success: {0:d}'.format(count['success']))
	print('      failure: {0:d}'.format(count['failure']))
	sys.stderr.write('\n[{0:s}] Finished checking {1:d} addresses:\n'.format(now(), count['checked']))
	sys.stderr.write('      success: {0:d}\n'.format(count['success']))
	sys.stderr.write('      failure: {0:d}\n'.format(count['failure']))
	sys.stderr.write('Statuses: {0}\n'.format(statuses))


main()