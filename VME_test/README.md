# VME addressing tester

This is a simple command-line program for testing USB connection to VME modules and their addressing using read/write commands.

### Technical information

The program is written in C, and uses the official `CAENComm` library from [CAEN](http://www.caen.it).  
To compile the program, use the following command:

    gcc ./VME_TestRW.c -l CAENComm -o ./VME_TestRW.exe
