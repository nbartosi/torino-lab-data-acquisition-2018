
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "CAENVMElib.h"
#define NAF ((N<<10)+(A<<6)+((F&0xf)<<2))

/*
    -----------------------------------------------------------------------------

      Main program

    -----------------------------------------------------------------------------
*/

int main ()

{
CVBoardTypes  VMEBoard = cvV1718;
short         Link = 0;
short         Device;
long          BHandle;
int           N,A,F,base,add;
int           data,err,lw;
int           q,x;
int           xread,xwrite,xdum;
unsigned short xread16,xwrit16,xdum16;

base=0x550000;
q=0;

// Initialize the Board
if( CAENVME_Init(VMEBoard, Device, Link, &BHandle) != cvSuccess ) 
    {
   printf("\n\n Error opening the device\n");
    exit(1);
}

 for (;;){

printf("\n \nPer uscire inserire N<0 \n");
printf("Inserire N: ");
scanf("%d",&N);
 if (N<0) break;
printf("Inserire A: ");
scanf("%d",&A);
printf("Inserire F: ");
scanf("%d",&F);
printf("Inserire data size (long word=1, word=0): ");
scanf("%d",&lw);
printf("Dati inseriti: N=%d, A=%d, F=%d \n",N,A,F);;
add=base+NAF;

if (F>=16) {
   printf("Inserire dato: ");
   scanf("%x",&data);
   //printf("Inserire data size : long word=1, word=0\n");
   //scanf("%d",&lw);
   if(lw==1)   
     err=CAENVME_WriteCycle(BHandle,add,&data,cvA24_U_DATA,cvD32);
   if(lw==0)
     err=CAENVME_WriteCycle(BHandle,add,&data,cvA24_U_DATA,cvD16);   
   printf("Status: %d ",err);
 } 

 if (F<16) { 
   //printf("Inserire data size : long word=1, word=0\n");
   //scanf("%d",&lw);
   if(lw==1) {   
     err=CAENVME_ReadCycle(BHandle,add,&data,cvA24_U_DATA,cvD32);
     q = (data & 0x80000000) ? 1 : 0 ;
     data=(data & 0xffffff);
   }
   if(lw==0) {
     err=CAENVME_ReadCycle(BHandle,add,&data,cvA24_U_DATA,cvD16);
     //data=(data & 0xffff);
   }
   printf("\nDato letto: %x",data);
   printf("\nRisposta Q: %d",q);
   printf("\nStatus: %d ",err);
 }
 }

CAENVME_End(BHandle);
 return 0;
}

