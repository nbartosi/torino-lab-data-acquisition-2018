#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <errno.h>
#include <iostream.h>
#include <fstream>


//#include "/usr/local/root/include/TROOT.h"
#include "/usr/local/root/include/TFile.h"
#include "/usr/local/root/include/TH1.h"
#include "/usr/local/root/include/TH2.h"
#include "/usr/local/root/include/TProfile.h"
#include "/usr/local/root/include/TNtuple.h"
#include "/usr/local/root/include/TCanvas.h"
#include "/usr/local/root/include/TApplication.h"

#include "libcc32.h" /* the header of the shared library */

#define DEVICE_NAME "/dev/cc32_1"

char *cszPrgName;
CC32_HANDLE handle;
char *fname = DEVICE_NAME;

int iev, nevmax=100, nevfreq=1;

unsigned int n, a, fun;
int q,x;
unsigned int xread,xwrit;
unsigned short xread16,xwrit16;
int i;
float one;
int error;
int ntime;
int igood=0;


TH1F *h1;
TCanvas *MyC;
TFile *fhis;



int main()
{
//TROOT root("simple","Test of histogramming and I/O");
   TApplication app("app",0,0);
 
   int nevmax =0;
   cout << "Enter the number of events: " << endl;
   cin >> nevmax;
   cout << "Enter data file name: " << endl;
   char fileName[99];
   cin >> fileName;
//    file_name = "dati/" + fileName;
   ofstream file_out(fileName);
   
   error=cc32_open(fname,&handle);
   if(error) {
        fprintf(stderr, "%s: %s\n", fname, strerror(error));
        exit(1);
   }
   else
    	printf("open done.\n");

//   fhis = new TFile ("daq.root","recreate");
   h1 = new TH1F("h1","spectrum",100,0.,100.);
   TCanvas *MyC = new TCanvas("MyC","mu lifetime",0,0,600,600);
   MyC->cd();
   h1->Draw();
   MyC->Update();


/* -- Z  and remove inhibit*/
    cc32_write_word(handle,0,1,16,0);
    printf("Initialize crate\n");
    cc32_write_word(handle,0,2,16,0);
    printf("Remove Inhibit from crate\n");
    
    printf("Initialize Status A\n");
    xwrit=0x108;
    cc32_write_long(handle,23,0,17,xwrit);
    q=0;
//  reset scaler
    xread=cc32_read_long(handle,20,4,10,&q,&x); // clear scaler

    for (iev = 0; iev < nevmax; iev++) {
// wait for the trigger from STATUSA
	 q=0;
	 while(q==0) xread=cc32_read_long(handle,23,3,8,&q,&x);
	 usleep(1000);
	 ntime=0;
         xread=cc32_read_long(handle,20,4,0,&q,&x); //read scaler
	 if(xread<68){
	 igood++;
	 h1->Fill(xread);
	 file_out << xread << endl;
     if(iev%nevfreq==0) {
       printf("cycle no. %d eventi buoni =%d\n",iev,igood);
       printf("data =%x %d,q=%d\n",xread,xread,q);
       h1->Draw();
       MyC->Update();
    	}
     }
         xread=cc32_read_long(handle,20,4,9,&q,&x); // clear scaler
         xread=cc32_read_long(handle,23,3,10,&q,&x); //reset STATUSA
   }

   cc32_close(handle);
   printf("close done.\n");
//   h1->Write();
   app.Run();
//   return(0);
}
