#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <errno.h>

/*
//#include "/usr/local/root/include/TROOT.h"
#include "/usr/local/root/include/TFile.h"
#include "/usr/local/root/include/TH1.h"
#include "/usr/local/root/include/TH2.h"
#include "/usr/local/root/include/TProfile.h"
#include "/usr/local/root/include/TNtuple.h"
#include "/usr/local/root/include/TCanvas.h"
#include "/usr/local/root/include/TApplication.h"
*/

//#include "libcc32.h" /* the header of the shared library */
//#define DEVICE_NAME "/dev/cc32_1"

#include "CAENVMElib.h"
#define NAF ((N<<10)+(A<<6)+((F&0xf)<<2))

CVBoardTypes  VMEBoard = cvV1718;
short         Link = 0;
short         Device;
long           BHandle;
int           N,A,F,base,add;
int           data,err,lw;

/*
char *cszPrgName;
CC32_HANDLE handle;
char *fname = DEVICE_NAME;
*/

int iev, nevmax=10, nevfreq=1;
int q,x;
unsigned int xread,xwrit;
unsigned short xread16,xwrit16;
int i;
float one;
int error;
int ntime;
int igood=0;

/*
TH1F *h1;
TCanvas *MyC;
TFile *fhis;
*/


int main()
{

base=0x550000;

// Initialize the Board
if( CAENVME_Init(VMEBoard, Device, Link, &BHandle) != cvSuccess ) 
    {
   printf("\n\n Error opening the device\n");
    exit(1);
}
//TROOT root("simple","Test of histogramming and I/O");
//   TApplication app("app",0,0);
 
   char fileName[99];
   int nevmax =0;
   printf("Enter the number of events: \n");
//   cout << "Enter the number of events: " << endl;
   scanf("%d",&nevmax);
//   cin >> nevmax;
//   cout << "Enter data file name: " << endl;
   printf("Enter data file name: \n");
   scanf("%s",fileName);
   printf("file name for output = %s\n",fileName);
//   cin >> fileName;
//   ofstream file_out(fileName);
   
/*
   error=cc32_open(fname,&handle);
   if(error) {
        fprintf(stderr, "%s: %s\n", fname, strerror(error));
        exit(1);
   }
   else
    	printf("open done.\n");
*/

/*
//   fhis = new TFile ("daq.root","recreate");
   h1 = new TH1F("h1","spectrum",100,0.,100.);
   TCanvas *MyC = new TCanvas("MyC","mu lifetime",0,0,600,600);
   MyC->cd();
   h1->Draw();
   MyC->Update();
*/

/* -- Z  and remove inhibit*/
//    cc32_write_word(handle,0,1,16,0);
    data=0;
    N=0; A=1; F=16;
    add=base+NAF;
    err=CAENVME_WriteCycle(BHandle,add,&data,cvA24_U_DATA,cvD16);   
    printf("Status: %d ",err);
    printf("Initialize crate\n");
//    cc32_write_word(handle,0,2,16,0);
    N=0; A=2; F=16;
    add=base+NAF;
    err=CAENVME_WriteCycle(BHandle,add,&data,cvA24_U_DATA,cvD16);   
    printf("Status: %d ",err);
    printf("Remove Inhibit from crate\n");
    
    printf("Initialize Status A\n");
    data=0x0000ffff;
    N=23; A=0; F=17;
    add=base+NAF;
    err=CAENVME_WriteCycle(BHandle,add,&data,cvA24_U_DATA,cvD16);   
    printf("Status after STA init: %d \n",err);
//    cc32_write_long(handle,23,0,17,xwrit);
    N=23; A=0; F=1;
    add=base+NAF;
    err=CAENVME_ReadCycle(BHandle,add,&data,cvA24_U_DATA,cvD32);   
    printf("Status: %d \n",err);
    q = (data & 0x80000000) ? 1 : 0 ;
    xread=(data & 0xffff);
    printf("reading Status A setup: data = %x  Q = %d\n",xread,q);
//    cc32_write_long(handle,23,0,1,xwrit);
    
//  reset scaler
    N=9; A=0; F=10;
    add=base+NAF;
    err=CAENVME_ReadCycle(BHandle,add,&data,cvA24_U_DATA,cvD32);
    printf("Status after clearing scaler: %d \n",err);
//    xread=cc32_read_long(handle,20,4,10,&q,&x); // clear scaler

    for (iev = 0; iev < nevmax; iev++) {
// wait for the trigger from STATUSA
    printf("loop over events: event no. %d\n",iev);
    q=0; ntime=0;
    N=23; A=0; F=8;
    add=base+NAF;
    while(q==0) {
//xread=cc32_read_long(handle,23,3,8,&q,&x);
      err=CAENVME_ReadCycle(BHandle,add,&data,cvA24_U_DATA,cvD32);
      q = (data & 0x80000000) ? 1 : 0 ;
//      printf("Q response to F8 on STA = %d\n",q);
//      data=(data & 0xffffff);
      ntime++;
//      if(ntime>1000) break;
      usleep(10000);
     if(ntime%1000==0) printf("waiting for trigger!\n");

    }
    usleep(1000);
    ntime=0;
    N=9; A=15; F=0;
    add=base+NAF;
    err=CAENVME_ReadCycle(BHandle,add,&data,cvA24_U_DATA,cvD32);
    xread=(data & 0xffff);
//         xread=cc32_read_long(handle,20,4,0,&q,&x); //read scaler
	 if(xread<68){
	 igood++;
//	 h1->Fill(xread);
//	 file_out << xread << endl;
     if(iev%nevfreq==0) {
       printf("cycle no. %d eventi buoni =%d\n",iev,igood);
       printf("data =%x %d,q=%d\n",xread,xread,q);
/*
       h1->Draw();
       MyC->Update();
*/
    	}
     }
    N=9; A=15; F=9;
    add=base+NAF;
    err=CAENVME_ReadCycle(BHandle,add,&data,cvA24_U_DATA,cvD32);
    N=23; A=0; F=10;
    add=base+NAF;
    err=CAENVME_ReadCycle(BHandle,add,&data,cvA24_U_DATA,cvD32);
//         xread=cc32_read_long(handle,20,4,9,&q,&x); // clear scaler
//         xread=cc32_read_long(handle,23,3,10,&q,&x); //reset STATUSA
   }

//   cc32_close(handle);
CAENVME_End(BHandle);
   printf("close done.\n");
//   h1->Write();
//   app.Run();
   return(0);
}
