/* 
 * File:   main.c
 * Author: giorgione
 *
 * Created on May 25, 2017, 11:25 AM
 *
 * Questo programma nasce con scopi didattici ed e' pensato per gli studenti.
 * Nel corso del laboratorio gli studenti dapprima lo utilizzeranno per provare
 * i singoli moduli VME e comprenderne il funzionamento e, in seguito, auspico
 * possa servire come punto di partenza per scrivere autonomamente una parte
 * del programma di acquisizione dati da utilizzare nelle esperienze di fisica
 * che si svolgeranno nel corso del laboratorio.
 * 
 * Data la sua semplicita' (non ho creato header files) consiglio di compilarlo
 * direttamente senza utilizzare il makefile.
 * 
 * Questo e' il comando per compilarlo:
 * gcc ./VME_TestRW.c -l CAENComm -o ./VME_TestRW.exe
 * 
 * Per suggerimenti o domande potete scrivermi all'indirizzo
 *  giorgio.cotto@unito.it
 * 
 * Buon lavoro.
 * 
 * Giorgio Cotto
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

// librerie CAEN specifiche per VME.
//
// CAENComm
// le funzioni contenute nella libreria CAENComm sono un'astrazione del generico
// modulo VME. Esse, infatti, permettono di trattare ogni singolo modulo come
// entita' a se stante ed indirizzarlo come elemento unico ed indipendente dal
// bridge VME-USB mod. V1718.
// Le CAENCommonLib sono utilizzate in alternativa alle CAENVMElib,
// che gestiscono solo la connessione con il bridge USB e si collocano quindi
// ad un livello inferiore di astrazione.
#include <CAENComm.h>

// CAENVMElib
// contiene le funzioni che gestiscono la comunicazione con il brigde VME-USB.
// Sono utilizzate dalle CAENComm e quindi si collocano ad un livello inferiore
// di astrazione. Per essere piu' precisi si puo' affermare che forniscono
// al programmatore l'interfaccia verso il modulo del kernel che gestisce la
// conssessione USB con il bridge. In questo programma non vengono utilizzate.
//#include <CAENVMElib.h>

// Creo un nuovo tipo (statement: typedef) di struttura (struct) e la chiamo tdVMEModule
// Detta struttura contiene i parametri necessari ad aprire e gestire la connessione
// con un generico modulo VME.
typedef struct {
    CAENComm_ConnectionType ConnType;     // USB link type
    int LinkNum;                          // USB V1718 magic number
    int ConetNode;                        // USB must be 0
    uint32_t BaseAddr;                    // base address
    int Handler;                          // connection handler
    uint32_t Addr;                        // si puo' usare per memorizzare
                                          // l'ultimo indirizzo usato
    char* AddAutoIncrement;               // auto-incremento dell'indirizzo
} tdVMEModule;



// dichiarazione del prototipo della funzione che decodifica gli errori e li
// stampa sull'interfaccia utente
void decodeCaenMyError(CAENComm_ErrorCode* MyError);
void decodeCaenMyError(CAENComm_ErrorCode* MyError) {
    char MyErrorString[500];
    bzero(MyErrorString, 500*sizeof(char));         // inizializzo MyErrorString
    CAENComm_DecodeError(*MyError, MyErrorString);
    printf("\nIl codice di errore e': %d\n"
           "\t==> %s <==\n\n", *MyError, MyErrorString);
    printf("\nPremi Invio per continuare\n");
    getchar();
}


// Legge un carattere dallo standard input (stdin). Se vengono premuti piu' di
// un carattere, si considera solo l'ultimo premuto.
// Nota: il carattere di EndOfInput e' 0x0A (Line Feed).
void readOneChar(char* userChoice);
void readOneChar(char* userChoice) {
    char cinput = NULL;
    while ((cinput = getchar()) != 0xA)
    {
        //fprintf(stdout, "il carattere letto e': %x\t%c\n", cinput, cinput);
        if (!(isspace(cinput))) { // isspace restituisce 0 se cinput != blank
            *userChoice = cinput;
        }
    }
}

// funzione che mostra la schermata utente con le diverse opzioni
void showOptions(tdVMEModule* MyModule, char* userChoice);
void showOptions(tdVMEModule* MyModule, char* userChoice) {
    system("clear");
    printf("\n\n"
            "Seleziona una funzione tra le seguenti:\n"
            " --> Base Address: [0x%.8X] <--\n\n"
            "\t1 - modifica Address [0x%.8X]\n\n"
            "\t-----------------------------------\n"
            "\t2 - Read[D16]\n"
            "\t3 - Read[D32]\n"
            "\t4 - Address auto-increment: [%s]\n"
            "\t-----------------------------------\n"
            "\t5 - Write[D16]\n"
            "\t6 - Write[D32]\n"
            "\t-----------------------------------\n"
            "\n\t0 - Quit\n"
            "\n\n\n"
            "\t? ", MyModule->BaseAddr, MyModule->Addr, MyModule->AddAutoIncrement);
    readOneChar(userChoice);
    if(!(isdigit(*userChoice))) {
        // e' stato premuto un tasto diverso da 0 --> 9
        printf("\t   ==> scelta non valida!\n\n");
        showOptions(MyModule, userChoice);
    }
}


/*
 * Questa e' la dichiarazione standard utilizzata per la funzione main.
 * argc e' il numero dei parametri che vengono passati (il minimo e' 1, cioe'
 * il path assoluto del programma)
 * argv e' un array di stringhe che contiene i parametri (flags) inseriti
 * dall'utente
 * 
 * NOTA: la funzione main e' l'eseguibile stesso che viene lanciato dall'utente
 * scrivendo nella shell il nome del programma e poi premendo invio.
 * Gli eventuali argomenti della funzione main vengono quindi passati
 * dall'utente sotto forma di "opzioni" (flags).
 */
int main(int argc, char** argv) {
    
    // variabili utilizzate nel corso del programma
    char strON[] = "ON";
    char strOFF[] = "OFF";
    char c;
    
    
    // creo la struct MyModule che conterra' i parametri per la connessione
    // al modulo VME e poi lo inizializzo
    tdVMEModule MyModule;
    MyModule.ConnType = CAENComm_USB;   // vedi documentazione CaenCommonLib
    MyModule.ConetNode = 0;          // vedi documentazione CaenCommonLib
    MyModule.Handler = 0;            // verra' assegnato da CAENComm_OpenDevice
    MyModule.LinkNum = 0;            // valore contenuto in argv[1]
    MyModule.BaseAddr = 0;           // valore contenuto in argv[2]
    MyModule.Addr = 0;               // valore che verra' inserito manualmente dall'utente
    MyModule.AddAutoIncrement = strOFF;  // stato dell'auto-incremento

    
    // CAENComm_ErrorCode e' un tipo definito in CAENComm.h ed e' il valore
    // restituito dalle funzioni della libreria CAENComm che verranno utilizzate
    // in questo programma. Il suddetto valore indica il successo (o l'insuccesso)
    // della funzione invocata.
    // Creo ed inizializzo la variabile MyError che conterra' il suddetto
    // codice di errore (0 = nessun errore).
    CAENComm_ErrorCode MyError = 0;

    // variabile in cui registrare la scelta dell'utente
    char userChoice = 0;
    char userString[50];
    bzero(userString, 50*sizeof(char));
    
    // variabile usata per memorizzare i valori scritti e letti sul modulo
    uint32_t moduleIOBuff = 0;


    // i parametri che voglio vengano passati sono il magic number del file di
    // device del bridge usb ed il base address nel formato 0x12345678.
    // Faccio quindi un check e, se non e' specificato termino l'eseguzione 
    // del programa con un messaggio di errore.
    if(argc == 3) {
        MyModule.LinkNum = atoi(argv[1]);   // stdlib: ascii to integer
        printf("il magic number inserito e' %d\n", MyModule.LinkNum);
        MyModule.BaseAddr = (uint32_t)strtoll(argv[2], NULL, 0);    // stdlib: string to long long + cast a uint32_t
        printf("il base address inserito e' 0x%.8X\n", MyModule.BaseAddr);
    } else {
        printf("\nQuesto programma serve per accedere ad un modulo VME.\n\n"
                "La sua sintassi e':\n"
                "\t./main <USB_magic_number> "
                "<module_base_addr_32bit_0x12345678>\n\n");
        return (EXIT_FAILURE);
    }

    // Tramite il bridge CAEN_USB di cui ho specificato il magic number (in pratica
    // il numero che identifica il file di device del suddetto bridge), apro
    // la connessione con il modulo VME che ha indirizzo BaseAddr. Questa
    // connessione e' identificata univocamente dal numero Handler (di tipo int
    // e di cui la funzione richede il puntatore) e che viene assegnato all'atto
    // della connessione.

    MyError = CAENComm_OpenDevice(MyModule.ConnType,
                                  MyModule.LinkNum,
                                  MyModule.ConetNode,
                                  MyModule.BaseAddr,
                                  &MyModule.Handler);


    // Verifico che la connessione sia andata a buon fine
    if (MyError != 0) {
        // si e' verificato un errore
        decodeCaenMyError(&MyError);    // decodifico l'errore
//        return (EXIT_FAILURE);          // termino l'esecuzione del programma
    }

    
    // Qui inizia il ciclo while che gestisce il programma.
    // Viene eseguito un numero imprecisato di volte (1 significa True)
    while(1) {
        // azzero le variabili che uso per memorizzare l'input dell'utente
        bzero(&userChoice, sizeof(char));
        bzero(userString, 50*sizeof(char));
        bzero(&moduleIOBuff, sizeof(uint32_t));
        
        showOptions(&MyModule, &userChoice);     // mostro opzioni
        
        switch(userChoice) {
            // modifica Address
            case '1':
                printf("\n\t\tinserisci il nuovo Address nel formato 0x12345678\n"
                        "\n\t? ");
                scanf(" %s", userString);
                MyModule.Addr = (uint32_t)strtoll(userString, NULL, 0);
                
                while((c = getchar()) != '\n' && c != EOF);     // svuota input buffer
                printf("\nnuovo Addr: [0x%.8X]\n", MyModule.Addr);
                printf("\nPremi Invio per continuare\n");
                getchar();
                break;
                
            // Read D16
            case '2':
                // faccio una lettura D16 e metto l'output nella variabile moduleIOBuff.
                // Notare che faccio un cast a moduleIOBuff perche' e' dichiarata come uint32_t
                // ma in questo momento la uso come uint16_t.
                // N.B. CAENComm_Read16 accetta come terzo argomento un puntatore del tipo uint16_t*
                // quindi prima ottengo il valore di memoria &moduleIOBuff e poi
                // faccio il cast (uint16_t*).
                // ==> vedi precedenza ed associativita' degli operatori
                MyError = CAENComm_Read16(MyModule.Handler, MyModule.Addr, (uint16_t*)&moduleIOBuff);
                    if (MyError != 0) {
                        decodeCaenMyError(&MyError);    // decodifico l'errore
                    } else {
                        printf("\nRead[D16] @ [0x%.8X]\t0x%.4X\n", MyModule.Addr, moduleIOBuff);
                        
                        // controlla se e' attivato l'incremento automatico
                        if(MyModule.AddAutoIncrement == strON) {
                            MyModule.Addr = (MyModule.Addr + 0x00000002);
                        }

                        printf("\nPremi Invio per continuare\n");
                        getchar();
                    }
                break;

            // Read D32
            case '3':
                MyError = CAENComm_Read32(MyModule.Handler, MyModule.Addr, &moduleIOBuff);
                if (MyError != 0) {
                    decodeCaenMyError(&MyError);    // decodifico l'errore
                } else {
                        printf("Read[D32] @ [0x%.8X]\t0x%.8X\n", MyModule.Addr, moduleIOBuff);
                        
                        // controlla se e' attivato l'incremento automatico
                        if(MyModule.AddAutoIncrement == strON) {
                            MyModule.Addr = (MyModule.Addr + 0x00000004);
                        }
                            
                        printf("\nPremi Invio per continuare\n");
                        getchar();
                }
                break;
            
            // attiva/disattiva l'auto-incremento
            // l'idea e' la seguente: devo controllare se l'auto-incremento e' 
            // settato. Per non andare a fare comparazioni ed assegnazioni con
            // stringhe riportanti lo stato dell'auto-incremento ho creato due
            // stringhe (strON e strOFF) in cui ho scritto "ON" ed "OFF".
            // Poiche' l'elemento AddAutoIncrement della struct MyModule e' di
            // tipo char*, lo faccio puntare al bisogno all'una o all'altra
            // stringa. Il vantaggio e' di fare un test sul valore di due puntatori
            // e quello di potere immediatamente accedere al valore puntato
            // (cioe' ad ON o OFF) in modo da poterlo agevolmente scrivere
            // sull'interfaccia utente.
            case '4':
                if(MyModule.AddAutoIncrement == strOFF) {
                    MyModule.AddAutoIncrement = strON;
                } else {
                    MyModule.AddAutoIncrement = strOFF;
                }
                break;
                
            // Write[D16]
            case '5':
                printf("\n\t\tWrite[D16]: inserisci la word da scrivere nel formato [0x1234] e premi invio\n"
                        "\n\t? ");

                scanf("%s", userString);                
                moduleIOBuff = (uint16_t)strtoll(userString, NULL, 0);
                while((c = getchar()) != '\n' && c != EOF);     // svuota input buffer                                

                MyError = CAENComm_Write16(MyModule.Handler, MyModule.Addr, (uint16_t)moduleIOBuff);
                    if (MyError != 0) {
                        decodeCaenMyError(&MyError);    // decodifico l'errore
                    } else {
                        printf("\nscritta la word [0x%.4X] all'indirizzo [0x%.8X]\n", moduleIOBuff, MyModule.Addr);
                    }

                    printf("\nPremi Invio per continuare\n");
                    getchar();
                break;

            // Write[D32]
            case '6':
                printf("\n\t\tWrite[D32]: inserisci la word da scrivere nel formato [0x12345678] e premi invio\n"
                        "\n\t? ");

                scanf("%s", userString);                
                moduleIOBuff = (uint32_t)strtoll(userString, NULL, 0);
                while((c = getchar()) != '\n' && c != EOF);     // svuota input buffer                                

                MyError = CAENComm_Write32(MyModule.Handler, MyModule.Addr, (uint32_t)moduleIOBuff);
                    if (MyError != 0) {
                        decodeCaenMyError(&MyError);    // decodifico l'errore
                    } else {
                        printf("\nscritta la word [0x%.8X] all'indirizzo [0x%.8X]\n", moduleIOBuff, MyModule.Addr);
                    }

                    printf("\nPremi Invio per continuare\n");
                    getchar();
                break;
                
            case '0':
                break;
        
        }
        
        if(userChoice=='0'){
            break;
        }
        
    }
    
    
    // Qui sono fuori dal while, segno che l'utente ha premuto 0
    // ed ha deciso quindi di chiudere il protramma.
    // Chiudo quindi la connessione con il modulo ed esamino
    // l'eventuale messaggio di errore.
    MyError = CAENComm_CloseDevice(MyModule.Handler);
    if (MyError != 0) {
        decodeCaenMyError(&MyError);    // decodifico l'errore
        return (EXIT_FAILURE);          // termino l'esecuzione del programma
    }

    
    return (EXIT_SUCCESS);
}


