// Compile: gcc ./camac_ping.c -l CAENVME -o ./camac_ping.exe -D LINUX

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <math.h>
#include <inttypes.h>
#include <stdint.h>
#include <ctype.h>

#include "CAENVMElib.h"

typedef struct {
    CVBoardTypes VMEBoard;
    int Link;                          // USB V1718 magic number
    int Device;                        // USB must be 0
    uint32_t BaseAddr;                 // base address
    uint32_t Offset;                   // address offset
    uint32_t Address;                  // final address: base + offset
    int Handler;                       // connection handler
    char* AddAutoIncrement;            // auto-increment of the address offset
} tdVMEModule;


void decodeCaenMyError(CVErrorCodes *MyError, char* msg);
void decodeCaenMyError(CVErrorCodes *MyError, char* msg) {
    const char* MyErrorString = CAENVME_DecodeError(*MyError);
    sprintf(msg, "%d: %s", *MyError, MyErrorString);
    printf("  %s\n", msg);
}


int main(int argc, char** argv) {
    
    // variabili utilizzate nel corso del programma
    char strON[] = "ON";
    char strOFF[] = "OFF";
    char c;
    char status[500];
    
    tdVMEModule MyModule;
    MyModule.VMEBoard = cvV1718;
    MyModule.Link = 0;
    MyModule.Device = 0;
    MyModule.BaseAddr = 0;
    MyModule.Offset = 0;
    MyModule.Address = 0;
    MyModule.Handler = 0;
    MyModule.AddAutoIncrement = strOFF;  // stato dell'auto-incremento

    CVErrorCodes MyError = 0;

    if(argc >= 4) {
        MyModule.Device = 0;
        MyModule.Link = 0;
        MyModule.BaseAddr = (uint32_t)strtoll(argv[1], NULL, 0);    // stdlib: string to long long + cast a uint32_t
        MyModule.Offset = (uint32_t)strtoll(argv[2], NULL, 0);    // stdlib: string to long long + cast a uint32_t
        MyModule.Address = MyModule.BaseAddr + MyModule.Offset;
    } else {
        printf("\nThis program tests addressing of modules by writing a word using F16 function.\n\n"
                "Syntax:\n"
                "\t./main "
                "<base address: 32bit 0x12345678> "
                "<address offset: 32bit 0x12345678> "
                "<command: r16|r24|w16|w24> "
                "[<word: 16bit or 24bit>]\n\n");
        return (EXIT_FAILURE);
    }

    // Opening connection
    MyError = CAENVME_Init(MyModule.VMEBoard, 
                           MyModule.Link, 
                           MyModule.Device, 
                           &MyModule.Handler);

    // Verifying connection status
    if (MyError != cvSuccess) {
        printf("Failed initializing\n");
        decodeCaenMyError(&MyError, status);
        return (1);
    }

    // Parse the word string
    uint32_t word = 0;
    bzero(&word, sizeof(uint32_t));
    if (argc >= 5) {
        word = (uint32_t)strtoll(argv[4], NULL, 0);
    }

    // Parse the command string
    char command[10];
    sprintf(command, "%s", argv[3]);

    if (strcmp(command, "w16") == 0) {
        // Writing the 15->0 bits of the word directly
        MyError = CAENVME_WriteCycle(MyModule.Handler, MyModule.Address, &word, cvA24_U_DATA, cvD16);
        if (MyError == cvSuccess)
            printf("Wrote word [0x%.4X] to address [0x%.8X]\n", word, MyModule.Address);
    } else
    if (strcmp(command, "w24") == 0) {
        // Writing the 31->16 bits to 24bit addressing and then 15->0 bits to 16bit addressing
        // Striping the 16 least significant bits using right shift by 16 places
        uint16_t word_stripped = (uint16_t)(word >> 16);
        // Setting address bit 1 to 0 for using 24bit addressing
        uint32_t address = MyModule.Address & ~(1UL << 1);
        MyError = CAENVME_WriteCycle(MyModule.Handler, address, &word_stripped, cvA24_U_DATA, cvD16);
        if (MyError == cvSuccess) {
            // Keeping the 16 least significant bits by casting a 32-bit number to 16-bit type
            word_stripped = (uint16_t)word;
            // Setting address bit 1 to 1 for using 16bit addressing
            address = MyModule.Address | (1UL << 1);
            MyError = CAENVME_WriteCycle(MyModule.Handler, address, &word_stripped, cvA24_U_DATA, cvD16);
            if (MyError == cvSuccess)
                printf("Wrote word [0x%.6X] to address [0x%.8X]\n", word, MyModule.Address);
        }
    } else
    if (strcmp(command, "r16") == 0) {
        // Reading the 31->16 bits using 24bit addressing and then 15->0 bits using 16bit addressing
        bzero(&word, sizeof(uint32_t));
        MyError = CAENVME_ReadCycle(MyModule.Handler, MyModule.Address, &word, cvA24_U_DATA, cvD16);
        if (MyError == cvSuccess)
            printf("Read word [0x%.4X] from address [0x%.8X]\n", word, MyModule.Address);
    } else 
    if (strcmp(command, "r24") == 0) {
        // Reading the 31->16 bits of the word directly
        bzero(&word, sizeof(uint32_t));
        uint16_t word_fraction = 0;
        // Setting address bit 1 to 0 for using 24bit addressing
        uint32_t address = MyModule.Address & ~(1UL << 1);
        MyError = CAENVME_ReadCycle(MyModule.Handler, address, &word_fraction, cvA24_U_DATA, cvD16);
        if (MyError == cvSuccess) {
            word = ((uint32_t)word_fraction) << 16;
            // Setting address bit 1 to 1 for using 16bit addressing
            address = MyModule.Address | (1UL << 1);
            MyError = CAENVME_ReadCycle(MyModule.Handler, address, &word_fraction, cvA24_U_DATA, cvD16);
            if (MyError == cvSuccess) {
                word += word_fraction;
                printf("Read word [0x%.6X] from address [0x%.8X]\n", word, MyModule.Address);
            }
        }
    }
    else {
        printf("Wrong command. Should be one of: r16|r24|w16|w24\n");
    }

    // Decoding the error
    if (MyError != cvSuccess) {
        printf("Command [%s] to address [0x%.8X] with word [0x%.6X] FAILED\n", command, MyModule.Address, word);
        decodeCaenMyError(&MyError, status);
        return (2);
    }

    // Closing connection
    MyError = CAENVME_End(MyModule.Handler);
    if (MyError != cvSuccess) {
        decodeCaenMyError(&MyError, status);
        printf("Failed closing connection\n");
        return (3);
    }
    return (0);
}